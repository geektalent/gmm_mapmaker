package com.geeklabs.mapmaker.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.Toast;

import com.geeklabs.mapmaker.R;
import com.geeklabs.mapmaker.sqllite.SQLLiteManager;
import com.geeklabs.mapmaker.util.AppState;
import com.geeklabs.mapmaker.util.MarkConstants;
import com.geeklabs.mapmaker.util.MarkingInfo;

public class SearchActivity extends Activity {

	private AutoCompleteTextView searchtypeautotext;
	private double currntUserLat;
	private double currntUserLong;

	public double getCurrntUserLat() {
		return currntUserLat;
	}

	public double getCurrntUserLong() {
		return currntUserLong;
	}

	@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
	} 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.searchtype);

		searchtypeautotext = (AutoCompleteTextView) findViewById(R.id.searchlocationtype);
		searchtypeautotext.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,
				MarkConstants.LOCATION_TYPES));

		currntUserLat = getIntent().getDoubleExtra("currntUserLat", 0.0);
		currntUserLong = getIntent().getDoubleExtra("currntUserLong", 0.0);

		searchtypeautotext.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				String itemAtPosition = (String) arg0.getItemAtPosition(arg2);

				List<MarkingInfo> listAdapaterInfo = new ArrayList<MarkingInfo>();

				listAdapaterInfo = SQLLiteManager.getInstance(getApplicationContext()).getLocationsByType(
						itemAtPosition);

				// Hide soft window and clear action view collapse
				getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(searchtypeautotext.getWindowToken(), 0);
				searchtypeautotext.clearFocus();

				if (listAdapaterInfo.isEmpty()) {
					runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(SearchActivity.this, "Oops! No locations found.", Toast.LENGTH_LONG).show();
						}
					});
				}

				final ListView listview = (ListView) findViewById(R.id.locationlist);
				listview.setAdapter(new MyAdapter(SearchActivity.this, listAdapaterInfo));

				listview.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {

						Object obj = listview.getItemAtPosition(position);
						MarkingInfo markingInfo = (MarkingInfo) obj;

						if (!AppState.INSTANCE.appgenFQN.isEmpty()) {
							Intent intent = null;
							try {
								intent = new Intent(getApplicationContext(), Class.forName(AppState.INSTANCE.appgenFQN));
								intent.putExtra("selectedSearchTypeMarkingInfo", markingInfo);
								Log.i("Search by type", "Successfully put the values of marking info by type");
								setResult(RESULT_OK, intent);
								finish();
							} catch (ClassNotFoundException e) {
								e.printStackTrace();
								Log.e("Error Search by type",
										"Error ocured while getting marking info" + e.getMessage());
							}
						} else {
							Intent intent = new Intent(getApplicationContext(), MapMakerActivity.class);
							intent.putExtra("selectedSearchTypeMarkingInfo", markingInfo);
							Log.i("Search by type", "Successfully put the values of marking info by type");
							setResult(RESULT_OK, intent);
							finish();
						}
					}
				});
			}
		});
		// To show drop down list of types
		searchtypeautotext.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				searchtypeautotext.showDropDown();
				Log.i("Search by type", "Successfully shown drop down by type");
				return false;
			}
		});
	}
}

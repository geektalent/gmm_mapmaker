package com.geeklabs.mapmaker.sqllite;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.geeklabs.mapmaker.util.AppInfo;
import com.geeklabs.mapmaker.util.AppState;
import com.geeklabs.mapmaker.util.MarkConstants;
import com.geeklabs.mapmaker.util.MarkingInfo;
import com.google.android.gms.maps.model.LatLng;

/**
 * App table {Columns in order _id - 0,  ApplicatioName - 1, AppCode - 2, Description - 3}
 * Marking info table {Columns in order _id - 0, LocationName - 1, LocationType - 2, Description -3, Latitude-4, Longitude-5 MarkedBy-6 ZoomLevel-7, AppID-8}
 * Draw area table {Columns in order _id - 0, Latitude, Longitude, AppID}
 */
public class SQLLiteManager extends SQLiteOpenHelper {

	private static SQLLiteManager mInstance = null;

	private static final String DATABASE_NAME = "GeekLabs_MakeMymApp";
	private static final int DATABASE_VERSION = 1;

	private SQLLiteManager(Context ctx) {
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static SQLLiteManager getInstance(Context ctx) {
		// Use the application context, which will ensure that you
		// don't accidentally leak an Activity's context.
		if (mInstance == null) {
			mInstance = new SQLLiteManager(ctx.getApplicationContext());
		}
		return mInstance;
	}

	// This method will be called first time when db is created
	@Override
	public void onCreate(SQLiteDatabase db) {
		// Create a App info table
		createAppDetailsInfoTableIfRequired(db);

		// Create a Marking info table
		createMarkingInfo(db);

		// Create a draw area work
		createDrawArea(db);
	}
	
	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		// Create tables again
		onCreate(db);
	}

	private void createAppDetailsInfoTableIfRequired(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS "
				+ SQLLite.APP_INFO_TABLE_NAME
				+ " (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ApplicatioName TEXT NOT NULL COLLATE NOCASE, AppCode TEXT NOT NULL, Description TEXT NOT NULL)");
		Log.i("App Info table", "App info  table is successfully created ");
	}

	private void createMarkingInfo(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS "
				+ SQLLite.MARK_INFO_TABLE_NAME
				+ " (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, LocationName TEXT NOT NULL COLLATE NOCASE,LocationType TEXT NOT NULL,"
				+ " Description TEXT,Latitude DOUBLE NOT NULL,Longitude DOUBLE NOT NULL ,MarkedBy TEXT NOT NULL,"
				+ " ZoomLevel FLOAT NOT NULL,AppID INTEGER NOT NULL, FOREIGN KEY(AppID) REFERENCES "
				+ SQLLite.APP_INFO_TABLE_NAME + "(Id))");	
		Log.i("Marking info table", "Marking info table is successfully created");
		
	}

	private void createDrawArea(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS "
				+ SQLLite.DRAW_INFO_TABLE_NAME
				+ " (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Latitude DOUBLE NOT NULL, Longitude DOUBLE NOT NULL, AppID INTEGER NOT NULL, FOREIGN KEY(AppID) REFERENCES "
				+ SQLLite.APP_INFO_TABLE_NAME + "(Id))");	
		Log.i("Draw Boundray", "Successfully got lat lngs for Draw Boundray");
	}
	
	public void saveDrawArea(LinkedList<LatLng> markings) {
		SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

		if (!isTableExists(SQLLite.DRAW_INFO_TABLE_NAME)) {
			throw new IllegalStateException("draw area table doesn't exist");
		}

		int appId = AppState.INSTANCE.appId;
		sqLiteDatabase.beginTransaction();
		for (LatLng latLng : markings) {
			sqLiteDatabase
				.execSQL("INSERT INTO "
						+ SQLLite.DRAW_INFO_TABLE_NAME
						+ " (Latitude,Longitude,AppID) VALUES('"
						+ latLng.latitude + "','"
						+ latLng.longitude + "','" + appId + "')");
			Log.i("Draw Boundray", "Successfully saved lat lngs for Draw Boundray");
		}
		sqLiteDatabase.setTransactionSuccessful();
		sqLiteDatabase.endTransaction();
	}
	
	public int getBoundaryId(String lat, String lng) {
		SQLiteDatabase writableDatabase = this.getWritableDatabase();
		Cursor cursor = writableDatabase.query(
				SQLLite.DRAW_INFO_TABLE_NAME, null, "AppID = ? AND Latitude=? AND Longitude=?", new String[] {""+AppState.INSTANCE.appId,
						lat, lng }, null, null, null);
		int boundaryId = 0;
		if (!cursor.moveToFirst()) {
			return boundaryId = cursor.getInt(0);
		}
		cursor.close();
		Log.i("Existing Draw Boundray", "Successfully got exsiting  Draw Boundray");
		return boundaryId;
	}
	
	public void deleteDrawArea() {
		int appId = AppState.INSTANCE.appId;
		SQLiteDatabase db = this.getWritableDatabase();

		db.execSQL("DELETE FROM " + SQLLite.DRAW_INFO_TABLE_NAME + " WHERE AppID = '" + appId + "'");
		Log.i("Delete Boundray", "Successfully deleted existing draw boundray");
	}
	
	public LinkedList<LatLng> getDrawAreaInfo() {
		LinkedList<LatLng> markings = new LinkedList<LatLng>();

		int appId = AppState.INSTANCE.appId;

		SQLiteDatabase writableDatabase = this.getWritableDatabase();
		Cursor cursor = writableDatabase.query(
				SQLLite.DRAW_INFO_TABLE_NAME, null, "AppID=?",
				new String[] { "" + appId }, null, null, null);
		
		cursor.moveToFirst();//Move to first record
		while (cursor.isAfterLast() == false) {
			markings.add(new LatLng(cursor.getDouble(1), cursor.getDouble(2)));
			cursor.moveToNext();
		}

		cursor.close();
		return markings;
	}

	public void saveAppDetailsInfo(String applicationname, String description, String appCode) {
		SQLiteDatabase db = this.getWritableDatabase();

		db.execSQL("INSERT INTO " + SQLLite.APP_INFO_TABLE_NAME
				+ " (ApplicatioName,AppCode,Description) VALUES('" + applicationname
				+ "','" + appCode + "','" + description + "')");
		Log.i("App info", "App info is successfully saved");

	}
	
	public void updateAppDetailsInfo(String applicationname, String description, int id) {
		SQLiteDatabase database = this.getWritableDatabase();
		database.execSQL("UPDATE " + SQLLite.APP_INFO_TABLE_NAME  + " set ApplicatioName='" +applicationname+ "'," + " Description='"+description+ "' where _id='" +id+ "'");
		Log.i("Update app info", "App info is successfully updated");
	}
	
	public void deleteAppInfo(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		db.execSQL("DELETE FROM " + SQLLite.APP_INFO_TABLE_NAME + " WHERE _id = '" + id + "'");
		Log.i("Delete app info", "App info is successfully deleted");
	}
	
	public Cursor getList() {
		String orderBy =  "ApplicatioName" + " ASC "; // should fix sorting order issue
		Cursor cursor = this.getWritableDatabase().query(
				SQLLite.APP_INFO_TABLE_NAME, null, null, null, null,
				null, orderBy);

		return cursor;// Expected by auto suggestion adapter
	}
	
	public Cursor fetchAppsByName(String appNameStartsWith) {
		String orderBy =  "ApplicatioName" + " ASC "; // should fix sorting order issue
		Cursor cursor = this.getWritableDatabase().query(
				SQLLite.APP_INFO_TABLE_NAME, null,
				" ApplicatioName like '%" + appNameStartsWith + "%' ", null,
				null, null, orderBy);
		cursor.moveToFirst();

		return cursor; // Expected by auto suggestion adapter
	}

	public AppInfo fetchAppById(String appId) {
		SQLiteDatabase writableDatabase = this.getWritableDatabase();
		Cursor cursor = writableDatabase.query(
				SQLLite.APP_INFO_TABLE_NAME, null, " _id=?",
				new String[] { "" + appId }, null, null, null);
		AppInfo appInfo = SQLLite.getAppInfo(cursor);
		
		return appInfo;
	}
	
	public AppInfo fetchAppByCode(String appCode) {
		SQLiteDatabase writableDatabase = this.getWritableDatabase();
		Cursor cursor = writableDatabase.query(
				SQLLite.APP_INFO_TABLE_NAME, null, "AppCode=?",
				new String[] { "" + appCode }, null, null, null);
		AppInfo appInfo = SQLLite.getAppInfo(cursor);
		return appInfo;
	}

	public boolean isAppByCodeExist(String appCode) {
		SQLiteDatabase writableDatabase = this.getWritableDatabase();
		Cursor cursor = writableDatabase.query(
				SQLLite.APP_INFO_TABLE_NAME, null, " AppCode=?",
				new String[] {appCode}, null, null, null);
		
		boolean isAppExist = cursor.moveToFirst();
		cursor.close();
		return isAppExist;
	}
	
	public AppInfo isAppByNameExist(String appName) {
		SQLiteDatabase writableDatabase = this.getWritableDatabase();
		Cursor cursor = writableDatabase.query(
				SQLLite.APP_INFO_TABLE_NAME, null, " ApplicatioName=?",
				new String[] {appName}, null, null, null);
		
		AppInfo appInfo = SQLLite.getAppInfo(cursor);
		Log.i("App by name", "Successfully checked app by name");
		return appInfo;
	}
			
	public void saveMarkingInfo(double actualLatitude, double actualLongitude,
			String locationName, String locationType,
			String description, String markedBy, float zoomLevel) {
		SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

		if (!isTableExists(SQLLite.MARK_INFO_TABLE_NAME)) {
			Log.e("Marking info", "Marking info table is does not exist");
			throw new IllegalStateException("Marking info table doesn't exist");
			
		}

		// Given type doesn't exist, take as an other type
		locationType = MarkConstants.getDefaultTypeForNonSpecifiedType(locationType);
		
		int appId = AppState.INSTANCE.appId;
		sqLiteDatabase
				.execSQL("INSERT INTO "
						+ SQLLite.MARK_INFO_TABLE_NAME
						+ " (LocationName,LocationType,Description,Latitude,Longitude,MarkedBy,ZoomLevel,AppID) VALUES('"
						+ locationName + "','" + locationType + "','"
						+ description + "','" + actualLatitude + "','"
						+ actualLongitude + "','" + markedBy + "','"+zoomLevel
						+ "','" + appId + "')");
		Log.i("Marking info", "Marking info is successfully saved");

	}
	
	public void deleteMarkInfo(int id) {
		SQLiteDatabase db = this.getWritableDatabase();

		db.execSQL("DELETE FROM " + SQLLite.MARK_INFO_TABLE_NAME
				+ " WHERE _id = '" + id + "'");
		Log.i("Delete Marking info", "Successfully deleted existing marking info");

		//db.close();
	}

	public int getMarkerId(String lat, String lng) {
		SQLiteDatabase writableDatabase = this.getWritableDatabase();
		Cursor cursor = writableDatabase.query(
				SQLLite.MARK_INFO_TABLE_NAME, null, "AppID = ? AND Latitude=? AND Longitude=?", new String[] {""+AppState.INSTANCE.appId,
														lat, lng}, null, null, null);
		int isMarkingIdExist = 0;
		if (cursor.moveToFirst()) {
			isMarkingIdExist = cursor.getInt(0);
		}
		cursor.close();
		return isMarkingIdExist;
	}
	
	public void updateMarkingInfo(String locationname, String locationType,
			String description, float zoomLevel, int id) {
		SQLiteDatabase database = this.getWritableDatabase();
		
		// Given type doesn't exist, take as an other type
		locationType = MarkConstants.getDefaultTypeForNonSpecifiedType(locationType);
		
		database.execSQL("UPDATE " + SQLLite.MARK_INFO_TABLE_NAME
				+ " set LocationName='" + locationname + "',"
				+ " LocationType='" + locationType + "'," + " Description='"
				+ description + "' where _id='" + id + "'");
		Log.i("Update marking info", "Marking info is Successfully updated");
	}

	public List<MarkingInfo> getAllAppMarkingInfos() {
		int appId = AppState.INSTANCE.appId;

		SQLiteDatabase writableDatabase = this.getWritableDatabase();
		Cursor cursor = writableDatabase.query(
				SQLLite.MARK_INFO_TABLE_NAME, null, "AppID=?",
				new String[] { "" + appId }, null, null, null);
		List<MarkingInfo> iterateMarkingCursor = SQLLite.iterateMarkingCursor(cursor);
		Log.i("Get Marking info", "Successfully got existing marking info");
		return iterateMarkingCursor;
	}
	
	public void clearAllAppMarkings() {
		SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
		sqLiteDatabase.execSQL("DELETE FROM "
				+ SQLLite.MARK_INFO_TABLE_NAME + " WHERE AppID = '"
				+ AppState.INSTANCE.appId + "'");
		Log.i("Clear Marking info", "Successfully cleared existing marking info from database");
	}	

	public Cursor fetchLocations(String locationNameStartsWith) {
		String orderBy =  "LocationName" + " ASC "; // should fix sorting order issue
		Cursor cursor = this.getWritableDatabase().query(
				SQLLite.MARK_INFO_TABLE_NAME, null,
				" AppID = ? AND LocationName like '%" + locationNameStartsWith + "%' ", new String[] {""+AppState.INSTANCE.appId },
				null, null, orderBy);
		cursor.moveToFirst();

		return cursor; // Returns cursor because, cursor expected by search cursor adapter
	}

	public Cursor getLocationNameList() {
		Cursor cursor = this.getWritableDatabase().query(
				SQLLite.MARK_INFO_TABLE_NAME, null, "AppID = ?", new String[] {""+AppState.INSTANCE.appId }, null,
				null, null);

		return cursor;// Returns cursor because, cursor expected by search cursor adapter
	}

	public List<MarkingInfo> getLocationsByType(String locationType) {
		String orderBy =  "LocationName" + " ASC "; // should fix sorting order issue
		SQLiteDatabase writableDatabase = this.getWritableDatabase();
		Cursor cursor = writableDatabase.query(SQLLite.MARK_INFO_TABLE_NAME, null,
				" LocationType = ? AND AppID = ?", new String[] { "" + locationType, ""+AppState.INSTANCE.appId }, null, null, 
				orderBy);
		List<MarkingInfo> iterateMarkingCursor = SQLLite.iterateMarkingCursor(cursor);
		return iterateMarkingCursor;
	}	
	
	public boolean isTableExists(String tableName) {
		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(
						"SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ? ",
						new String[] { "table", tableName });
		if (!cursor.moveToFirst()) {
			return false;
		}
		int count = cursor.getInt(0);
		cursor.close();

		//db.close();
		return count > 0;
	}
	
	public void executeQuery(String sql) {
		SQLiteDatabase writableDatabase = this.getWritableDatabase();
		writableDatabase.execSQL(sql);
	}
}

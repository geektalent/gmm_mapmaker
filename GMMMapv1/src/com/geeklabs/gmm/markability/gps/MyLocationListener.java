package com.geeklabs.gmm.markability.gps;

import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.geeklabs.gmm.markability.domain.AddMapOverlayItem;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class MyLocationListener implements LocationListener {
	
	private MapView mapView;
	private Drawable res;
	private Context context;

	public MyLocationListener(MapView mapView, Drawable drawable, Context context) {
		this.mapView = mapView;
		this.res = drawable;
		this.context = context;
	}
	
	@Override
	public void onLocationChanged(Location location) {
		String coordinates[] = {""+location.getLatitude(), ""+location.getLongitude()};
        double lat = Double.parseDouble(coordinates[0]);
        double lon = Double.parseDouble(coordinates[1]);
       
     
		GeoPoint currentGeoPoint = new GeoPoint((int) (lat * 1E6),
				(int) (lon * 1E6));
		
		List<Overlay> mapOverlays = mapView.getOverlays();
        
        OverlayItem currentGeoOverlayItem = new OverlayItem(currentGeoPoint, "User Current Location", "");

       // AddMapOverlayItem currentGeoOverlay = new AddMapOverlayItem(mapView, res, context);
		//currentGeoOverlay.addOverlay(currentGeoOverlayItem);
		//mapOverlays.add(currentGeoOverlay);
		

		MapController mapController = mapView.getController();
		//mapController.setZoom(25);
		mapController.animateTo(currentGeoPoint);
		//mapView.getOverlays().remove(currentGeoOverlay);

		mapView.invalidate();

	}

	@Override
	public void onProviderDisabled(String arg0) {
		// Do something here if you would like to know when the provider is
		// disabled by the user
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// Do something here if you would like to know when the provider is
		// enabled by the user
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
	}
}

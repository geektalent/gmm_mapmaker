package com.geeklabs.mapmaker.activity.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ApplicationStatusNotification extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent != null) {
			String mAction = intent.getAction();
			Log.i("Make my mApp", mAction);
			if (mAction == Intent.ACTION_PACKAGE_DATA_CLEARED) {
				// ToDo
			}
		}
	}

}

package com.geeklabs.mapmaker.util;

import android.util.Log;

public enum SlideOutMenu {
	
//	Slide out data in menu list
	NORMAL("Normal Map"), SATELLITE("Satellite Map"), HYBRID("Hybrid Map"), MARKMYLOCATION("Mark My location"),
	SEARCHBYTYPE("Search by Type");
	
	private String menuItem;

	SlideOutMenu(String menuItem) {
		this.menuItem = menuItem;
	}
	
	public String getMenuItem() {
		return menuItem;
	}
	
	public static SlideOutMenu getMenuItemEnum(String itemValue) {
		SlideOutMenu[] values = values();
		
		for (SlideOutMenu menuItem : values) {
		if (menuItem.getMenuItem().equals(itemValue)) {
			return menuItem;
		}
		}
		return null;
	}
}

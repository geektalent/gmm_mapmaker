package com.geeklabs.gmm.markability.util;

import java.io.Serializable;

public class MarkingInfo implements Serializable {
	private String name;
	private String type;
	private String description;
	private double longtitutde;
	private double lattitude;
	private int id;
	private int zoomlevel;
	private int appId;
	
	public int getAppId() {
		return appId;
	}
	
	public void setAppId(int appId) {
		this.appId = appId;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getLongtitutde() {
		return longtitutde;
	}

	public void setLongtitutde(double longtitutde) {
		this.longtitutde = longtitutde;
	}

	public double getLattitude() {
		return lattitude;
	}

	public void setLattitude(double lattitude) {
		this.lattitude = lattitude;
	}

	public int getZoomlevel() {
		return zoomlevel;
	}

	public void setZoomlevel(int zoomlevel) {
		this.zoomlevel = zoomlevel;
	}

}

package com.geeklabs.mapmaker.activity;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.geeklabs.mapmaker.R;
import com.geeklabs.mapmaker.util.GeoCoderUtil;
import com.geeklabs.mapmaker.util.MarkingInfo;
import com.google.android.gms.maps.model.LatLng;

public class MyAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	private List<MarkingInfo> list;
	private SearchActivity searchActivity;

	public MyAdapter(SearchActivity searchActivity, List<MarkingInfo> listAdapaterInfo) {
		this.searchActivity = searchActivity;
		mInflater = LayoutInflater.from((Context) searchActivity);
		this.list = listAdapaterInfo;
	}

	public int getCount() {
		return list.size();
	}
	
	public Object getItem(int position) {
		return list.get(position);
	}
	
	public long getItemId(int position) {
		return position;
	}
	// To get marking info on search by type
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.custom_row, null);
			holder = new ViewHolder();
			holder.txtName = (TextView) convertView.findViewById(R.id.lname);
			holder.txtId = (TextView) convertView.findViewById(R.id.l_id);
			holder.txtAddress = (TextView) convertView.findViewById(R.id.addressInfo);
			holder.txtDistance = (TextView) convertView.findViewById(R.id.distanceValue);
			convertView.setTag(holder);
			Log.i("Search by type", "Successfully shown marking info by search by type");
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		MarkingInfo markingInfo = list.get(position);
		
		String address = GeoCoderUtil.getAddress(new LatLng(markingInfo.getLattitude(), markingInfo.getLongtitutde()), convertView.getContext());
		
		holder.txtName.setText(markingInfo.getName());
		holder.txtId.setText(String.valueOf(markingInfo.getId()));
		holder.txtAddress.setText(address);
		
		
		String distanceByUnit = GeoCoderUtil.getDistanceByUnit(searchActivity.getCurrntUserLat(), searchActivity.getCurrntUserLong(), markingInfo.getLattitude(),
				markingInfo.getLongtitutde());
		Log.i("Distance", "Distance is successfully shown");
		holder.txtDistance.setText(distanceByUnit);

		return convertView;
	}

	static class ViewHolder {
		TextView txtName;
		TextView txtId;
		TextView txtAddress;
		TextView txtDistance;
	}
	}



package com.geeklabs.mapmaker.activity;

public final class ActivityResult {

	private ActivityResult() {}
	
	public final static int SAVE_MARKER_RESULT = 1;
	public final static int SEARCH_BY_TYPE_RESULT = 2;
	public final static int SIGNIN_RESULT = 3;
	public final static int SIGNOUT_RESULT = 4;
	public final static int GEN_APP_RESULT = 5;
	
	public static final int RESULT_MAP_ACTIVITY = 6;
	public static final int RESULT_CREAT_APP = 7;
	public static final int RESULT_DOWNLOAD = 8;
	
	// For oauth
	public static final int ACCOUNT_CODE = 1601;
	
}

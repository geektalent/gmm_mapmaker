package com.geeklabs.mapmaker.activity;

import static com.geeklabs.mapmaker.util.MapUtil.setupStrictMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnSuggestionListener;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.geeklabs.mapmaker.R;
import com.geeklabs.mapmaker.activity.location.LocationServices;
import com.geeklabs.mapmaker.boundary.MapMode;
import com.geeklabs.mapmaker.infowindow.MapWrapperLayout;
import com.geeklabs.mapmaker.infowindow.OnInfoWindowItemTouchListener;
import com.geeklabs.mapmaker.slideoutnavigation.ActionsContentView;
import com.geeklabs.mapmaker.sqllite.SQLLite;
import com.geeklabs.mapmaker.sqllite.SQLLiteManager;
import com.geeklabs.mapmaker.util.AddLocationTypeIcon;
import com.geeklabs.mapmaker.util.AppState;
import com.geeklabs.mapmaker.util.GeoCoderUtil;
import com.geeklabs.mapmaker.util.MapUtil;
import com.geeklabs.mapmaker.util.MarkingInfo;
import com.geeklabs.mapmaker.util.SlideOutMenu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

/**
 * This is responsible for showing up Google map api.
 */
public class MapMakerActivity extends FragmentActivity implements LocationListener, ConnectionCallbacks,
		OnConnectionFailedListener {

	protected GoogleMap googleMap;
	private ImageView slideOutButton;

	// A request to connect to Location Services
	private LocationRequest mLocationRequest;

	// Stores the current instantiation of the location client in this object
	private LocationClient mLocationClient;

	// User marked locations
	private Map<Marker, MarkingInfo> userMarkers = new HashMap<Marker, MarkingInfo>();
	private Marker currentSelectedMarker;

	private LatLng myLatLng;
	private double userLatitude, userLongitude;
	private float currentZoomLevel = 16; // default to 16
	private Marker longTappedMarkerForSave;
	
	protected ActionsContentView viewActionsContentView;

	// Draw area related variables
	private PolygonOptions polygonOptions = new PolygonOptions();
	private Polygon polygon;
	private LinkedList<LatLng> drawCoordinates = new LinkedList<LatLng>();
	protected MapMode mapMode = MapMode.MAP;
	private int mapType = GoogleMap.MAP_TYPE_NORMAL;
	protected ListView viewActionsList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_map);
		
		final ProgressDialog mapProgressBar = new ProgressDialog(MapMakerActivity.this);
		mapProgressBar.setMessage(" Please wait ...");
		mapProgressBar.show();
		mapProgressBar.setCancelable(false);
		
		try {
		// Check is network available
		checkNetworkAvailable(android.os.Process.myPid());

		// Make sure user device has n/w and gps enable
		if(!isGPSAvailable()) {
			showAlertDailog(this, "Improve your location by enabling GPS", "Enable GPS", NetworkType.GPS);
			Log.i("GPS", "GPS is disabled");
		}

		// Setup strict mode policies to detect problems while app is running
		setupStrictMode();

		// Create Location req and Location client
		setUpLocationClientIfNeeded();

		// create Google map instance
		initGoogleMap();		

		// Hide slideout on map click
		listenOnMapClick();

		// Init Draw area icons
		initDrawArea();
		toggleDrawIconVisiability(false);

		// on long press listener
		listenOnMapLongPress();

		//listenOnMyLocationChange - only for first time
		listenOnMyLocationChange();
		
		// keep screen active
		keepScreenActive();

		// On map touch - >
		listenOnCameraChange();

		// Setting pop up window i.e. info window
		setupInfoWindow();

		// slide out menu navigation
		setupSlideOut();
		} finally {
			mapProgressBar.cancel();
		}
	}

	@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
	} 
	
	private void listenOnMyLocationChange() {
		googleMap.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {
			
			@Override
			public void onMyLocationChange(Location location) {
				if (location != null) {
					onLocationChanged(location);
					MapUtil.animateMapToPositionAndZoom(googleMap, myLatLng, currentZoomLevel);
				}
				// for first time when map opens move camera to user location and never listen on this event, set it to null
				googleMap.setOnMyLocationChangeListener(null);
			}
		});
	}

	private void createLocReq() {
		if (mLocationRequest == null) {
			// Create a new global location parameters object
			mLocationRequest = LocationRequest.create();
			// Set the update interval
			mLocationRequest.setInterval(LocationServices.UPDATE_INTERVAL_IN_MILLISECONDS);
			// Use high accuracy
			mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
			// Set the interval ceiling to one minute
			mLocationRequest.setFastestInterval(LocationServices.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
		}
	}

	// To create draw area
	private void initDrawArea() {
		if (!isMapAvailable()) {
			return;
		}
		final ImageView save = (ImageView) findViewById(R.id.saveBoundary);
		final ImageView undo = (ImageView) findViewById(R.id.undoDraw);
		final ImageView clear = (ImageView) findViewById(R.id.clearBoundary);

		setStrokeWidth();
		
		if (drawCoordinates.isEmpty()) {
			SQLLiteManager manager = SQLLiteManager.getInstance(getApplicationContext());
			drawCoordinates = manager.getDrawAreaInfo();
		}
		drawPolygon();

		save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// first delete existing markers
				SQLLiteManager sqlLiteManager = SQLLiteManager.getInstance(getApplicationContext());
				sqlLiteManager.deleteDrawArea();

				// insert markings
				if (!drawCoordinates.isEmpty()) {
					sqlLiteManager.saveDrawArea(drawCoordinates);
					Log.i("Save boundary", "Draw boundary successfully saved");
				}

				setMapMode(MapMode.MAP); // Change map mode
				Log.i("Normal map mode", "Map mode is channged to normal mode");
			}
		});

		clear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (polygon != null && !drawCoordinates.isEmpty()) {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MapMakerActivity.this);

					// set title
					alertDialogBuilder.setTitle("Info");

					// set dialog message
					alertDialogBuilder.setMessage("Sure you want to remove the boundary permanently")
							.setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									// if this button is clicked, remove
									// plygon
									polygon.remove();
									polygon = null;
									if (!drawCoordinates.isEmpty()) {
										drawCoordinates.clear();
										Log.i("Clear boundary", "Successfully cleared Existing boundary on Map");
									}

									// delete boundary
									SQLLiteManager sqlLiteManager = SQLLiteManager.getInstance(MapMakerActivity.this);
									sqlLiteManager.deleteDrawArea();

									polygonOptions = new PolygonOptions();
									setStrokeWidth();
									setMapMode(MapMode.MAP); // Change
																// map
																// mode
									toggleDrawIconVisiability(false);
								}
							}).setNegativeButton("No", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									// if this button is clicked, just
									// close
									dialog.cancel();
								}
							});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
				}

			}
		});

		undo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (polygon != null & !drawCoordinates.isEmpty()) {
					polygon.remove(); // remove polygon

					// Remove last marker to re draw again
					drawCoordinates.removeLast();

					drawPolygon();
				}
			}
		});
	}

	// To draw boundray on map
	private void drawPolygon() {
		polygonOptions = new PolygonOptions(); // consider new options
		setStrokeWidth();
		
		for (LatLng latLng : drawCoordinates) {
			Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng).title(latLng.toString()));
			marker.setVisible(false);

			polygonOptions.add(marker.getPosition()).strokeColor(Color.RED);
			polygonOptions.fillColor(Color.TRANSPARENT);
			polygonOptions.visible(true);
		}
		List<LatLng> points = polygonOptions.getPoints();
		if (!points.isEmpty()) {
			polygon = googleMap.addPolygon(polygonOptions);
			Log.i("Poly lines","Successfully added polyline on map");
		}
	}

	private void setStrokeWidth() {
		polygonOptions.strokeWidth(reducePixels(MapMakerActivity.this, polygonOptions.getStrokeWidth()));
		Log.i("Poly width", "Successfully reduce poly line width");
	}
	
	// Setup map mode either Draw or Normal Map
	protected void setMapMode(MapMode mapMode) {
		this.mapMode = mapMode;
		if (mapMode == MapMode.DRAW) {
			toggleDrawIconVisiability(true);
		} else {
			toggleDrawIconVisiability(false);
		}
		
		// Update slide out menu
		viewActionsList.setAdapter(getSlideoutAdapter());
	}

	// To visible image buttons on map when map is in draw polygon mode
	protected void toggleDrawIconVisiability(boolean enableIcon) {
		final ImageView save = (ImageView) findViewById(R.id.saveBoundary);
		final ImageView undo = (ImageView) findViewById(R.id.undoDraw);
		final ImageView clear = (ImageView) findViewById(R.id.clearBoundary);

		if (enableIcon) {
			save.setVisibility(View.VISIBLE);
			undo.setVisibility(View.VISIBLE);
			clear.setVisibility(View.VISIBLE);
		} else {
			save.setVisibility(View.GONE);
			undo.setVisibility(View.GONE);
			clear.setVisibility(View.GONE);
		}
	}

	// set up Slide out
	private void setupSlideOut() {
		viewActionsContentView = (ActionsContentView) findViewById(R.id.actionsContentView);
		viewActionsContentView.setSwipingType(ActionsContentView.SWIPING_EDGE);

		viewActionsList = (ListView) findViewById(R.id.actions);
		// To add slide out menu items
		
		Log.i("Slide menus", "Successfully added slideout menus to slide");
		viewActionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View v, int position, long flags) {
				String itemAtPosition = (String) adapter.getItemAtPosition(position);
				showFragment(itemAtPosition);
			}
		});
		viewActionsList.setAdapter(getSlideoutAdapter());

		slideOutButton = (ImageView) findViewById(R.id.slide_button);
		// On button click show slide out menu
		slideOutButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (viewActionsContentView.isActionsShown()) {
					viewActionsContentView.showContent();
					Log.i("show content", "Successfully show conent when on click on slide out menu item");
				} else {
					viewActionsContentView.showActions();
				}
			}
		});

		// Don't show slide out on map open
		viewActionsContentView.showContent();
		Log.i("show content", "Successfully show conent when on click on slide out menu item");
	}

	protected ListAdapter getSlideoutAdapter() {
		final List<String> values = getSlideMenuItems();
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
				android.R.id.text1, values);
		return adapter;
	}

	// Slide out menu items
	protected List<String> getSlideMenuItems() {
		List<String> values = new ArrayList<String>();

		values.add(SlideOutMenu.NORMAL.getMenuItem());
		values.add(SlideOutMenu.SATELLITE.getMenuItem());
		values.add(SlideOutMenu.HYBRID.getMenuItem());
		values.add(SlideOutMenu.MARKMYLOCATION.getMenuItem());
		values.add(SlideOutMenu.SEARCHBYTYPE.getMenuItem());
		Log.i("Slide out menus", "successfully added slide out menu items on slide out");

		return values;
	}

	// To show markers by type
	private void showSearchByTypeMarker(Intent intent) {
		Serializable serializableExtra = intent.getSerializableExtra("selectedSearchTypeMarkingInfo");
		if (serializableExtra != null) {
			MarkingInfo selectedSearchLocation = (MarkingInfo) serializableExtra;

			Marker marker = getMarker(selectedSearchLocation.getId());
			if (marker != null) {
				MapUtil.animateMapToPositionAndZoom(googleMap, selectedSearchLocation.getPosition(),
						selectedSearchLocation.getZoomlevel());
				MapUtil.showInfoWindow(marker);
				marker.setDraggable(false);
			}
		}
	}

	private Marker getMarker(int markerId) {
		Marker marker = null;
		Set<Entry<Marker, MarkingInfo>> entrySet = userMarkers.entrySet();
		for (Entry<Marker, MarkingInfo> entry : entrySet) {
			MarkingInfo markingInfo = entry.getValue();
			int id = markingInfo.getId();
			if (markerId == id) {
				marker = entry.getKey();
				break;
			}
		}
		return marker;
	}

	private boolean checkNetworkAvailable(final int processId) {
		ConnectivityManager conMgr = getConnManager();
		boolean isMoblieDataEnabled = conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
		boolean isWifiEnabled = conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
		if (!isMoblieDataEnabled && !isWifiEnabled && googleMap == null) {

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
			// set title
			alertDialogBuilder.setTitle("Warning");
			alertDialogBuilder.setMessage("Check your network connection and try again");
			// set dialog message
			alertDialogBuilder.setCancelable(false).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					finish();
					moveTaskToBack(true);
				}
			});
			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			return false;
		}
		return true;
	}

	// To check whether GPS,Wifi or Mobile data are available or not
	private boolean isGPSAvailable() {
		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	private ConnectivityManager getConnManager() {
		ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(
				Context.CONNECTIVITY_SERVICE);
		return conMgr;
	}

	// To show alerts for GPS and Network services
	@SuppressWarnings("deprecation")
	private void showAlertDailog(MapMakerActivity mapMakerActivity, String message, String buttonLabel,
			final NetworkType networkType) {
		AlertDialog builder = new AlertDialog.Builder(this).create();

		builder.setMessage(message);

		builder.setButton(buttonLabel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				if (NetworkType.GPS == networkType) {
					MapUtil.showGpsOptions(MapMakerActivity.this);
					Log.i("Gps settings", "Gps settings successfully shown");
				} else {
					MapUtil.showWifiOptions(MapMakerActivity.this);
					Log.i("Wifi settings", "Wifi settings successfully shown");
				}
			}
		});

		builder.setButton2("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				return;
			}
		});

		builder.show();
	}

	enum NetworkType {
		GPS, WIFI, MOBILE
	}

	// Actions for slide out menu items
	protected boolean showFragment(String itemAtPosition) {
		SlideOutMenu slideOutMenu = SlideOutMenu.getMenuItemEnum(itemAtPosition);
		if (slideOutMenu == null) {
			return false;
		}
		switch (slideOutMenu) {
		case NORMAL:
			mapType = GoogleMap.MAP_TYPE_NORMAL;
			googleMap.setMapType(mapType);
			clearAndReloadMap(mapType);
			break;
		case HYBRID:
			mapType = GoogleMap.MAP_TYPE_HYBRID;
			googleMap.setMapType(mapType);
			clearAndReloadMap(mapType);
			break;
		case SATELLITE:
			mapType = GoogleMap.MAP_TYPE_SATELLITE;
			googleMap.setMapType(mapType);
			clearAndReloadMap(mapType);
			break;
		case MARKMYLOCATION:
			String markedBy = getMarkedBy();
			Intent intent = new Intent(this, MarkerSaveFormActivity.class);
			intent.putExtra("MarkedBy", markedBy);
			intent.putExtra("zoomLevel", currentZoomLevel);
			intent.putExtra("myLocationLatitude", userLatitude);
			intent.putExtra("myLocationLongitude", userLongitude);
			this.startActivityForResult(intent, ActivityResult.SAVE_MARKER_RESULT);
			break;

		case SEARCHBYTYPE:
			Intent i = new Intent(getApplicationContext(), SearchActivity.class);
			i.putExtra("currntUserLat", userLatitude);
			i.putExtra("currntUserLong", userLongitude);
			startActivityForResult(i, ActivityResult.SEARCH_BY_TYPE_RESULT);
			break;
		default:
			return false;
		}

		viewActionsContentView.showContent();
		return true;
	}

	private void clearAndReloadMap(int mapType) {
		googleMap.clear();
		int color = Color.BLUE;
		if (mapType != GoogleMap.MAP_TYPE_NORMAL) { // if not normal map , color will be white, else blue
			color = Color.WHITE;
		}
		addExistingMarkers(color);		
		
		initDrawArea();
	}

	// To keep screen on until app is closed
	private void keepScreenActive() {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	public int getPixelsFromDp(Context context, float dp) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dp * scale + 0.5f);
	}

	private void setupInfoWindow() {

		final MapWrapperLayout mapWrapperLayout = (MapWrapperLayout) findViewById(R.id.marker_info_window);
		// MapWrapperLayout initialization
		// 39 - default marker height
		// 20 - offset between the default InfoWindow bottom edge and it's
		// content bottom edge
		mapWrapperLayout.init(googleMap, getPixelsFromDp(this, 50 + 20));

		// We want to reuse the info window for all the markers, so let's create
		// only one class member instance

		final ViewGroup infoWindow = (ViewGroup) getLayoutInflater().inflate(R.layout.marker_info_window, null);

		final TextView title = (TextView) infoWindow.findViewById(R.id.marker_name);
		final TextView description = (TextView) infoWindow.findViewById(R.id.description);
		final TextView address = (TextView) infoWindow.findViewById(R.id.Address);
		final TextView distanceView = (TextView) infoWindow.findViewById(R.id.DistanceValue);
		final TextView discriptionLabel = (TextView) infoWindow.findViewById(R.id.descriptionlabel);
		final TextView locationLabel = (TextView) infoWindow.findViewById(R.id.Location);

		final ImageView deleteButton = (ImageView) infoWindow.findViewById(R.id.balloon_delete);
		final ImageView editButton = (ImageView) infoWindow.findViewById(R.id.balloon_edit);
		final ImageView getMeHere = (ImageView) infoWindow.findViewById(R.id.get_me_here);
		final ImageView markHere = (ImageView) infoWindow.findViewById(R.id.mark_here);

		// Set onTouch listener on mark here
		final OnInfoWindowItemTouchListener markHereListener = setOnTouchListenerOnMarkHere(markHere);

		// Set onTouch listener on Get me here
		final OnInfoWindowItemTouchListener getMeHereListener = setOnTouchListenerOnGetMeHere(getMeHere);

		// Set onTouch listener on Edit
		final OnInfoWindowItemTouchListener editListener = setOnTouchListenerEditButton(editButton);

		// Set onTouch listener on delete
		final OnInfoWindowItemTouchListener deleteListener = setOnTouchListenerOnDeleteButton(deleteButton);

		googleMap.setInfoWindowAdapter(new InfoWindowAdapter() {

			@Override
			public View getInfoWindow(Marker arg0) {
				return null;
			}

			@Override
			public View getInfoContents(final Marker marker) {
				// Getting view from the layout file info_window_layout
				// Getting the position from the marker
				final MarkingInfo markingInfo = userMarkers.get(marker);
				// We must call this to set the current marker and infoWindow
				// references
				// to the MapWrapperLayout
				title.setText(marker.getTitle());

				MapMakerActivity.this.currentSelectedMarker = marker;

				mapWrapperLayout.setMarkerWithInfoWindow(marker, infoWindow);

				String distanceByUnit = GeoCoderUtil.getDistanceByUnit(userLatitude, userLongitude,
						currentSelectedMarker.getPosition().latitude, currentSelectedMarker.getPosition().longitude);
				if (markingInfo != null) { // Onclick on our marker
					getMeHereListener.setMarker(marker);
					editListener.setMarker(marker);
					deleteListener.setMarker(marker);
					markHereListener.setMarker(marker);

					toggleBetweenDeleteEditButtons(markingInfo.getMarkedBy(), deleteButton, editButton);

					markHere.setVisibility(View.GONE);
					if (markingInfo.getDescription().toString().equals("")) {
						discriptionLabel.setVisibility(View.GONE);
						description.setVisibility(View.GONE);
					} else {
						discriptionLabel.setVisibility(View.VISIBLE);
						description.setVisibility(View.VISIBLE);
						description.setText(markingInfo.getDescription());
					}
					locationLabel.setVisibility(View.VISIBLE);
					title.setVisibility(View.VISIBLE);

					address.setText(GeoCoderUtil.getAddress(marker.getPosition(), MapMakerActivity.this));
					distanceView.setText(distanceByUnit);

				} else {
					// This case is when user long click on some where else on
					// map.
					markHereListener.setMarker(marker);
					getMeHereListener.setMarker(marker);

					address.setText(GeoCoderUtil.getAddress(marker.getPosition(), MapMakerActivity.this));
					distanceView.setText(distanceByUnit);
					marker.setVisible(false);
					marker.setDraggable(false);
					
					// remove buttons
					deleteButton.setVisibility(View.GONE);
					editButton.setVisibility(View.GONE);
					markHere.setVisibility(View.VISIBLE);
					title.setVisibility(View.GONE);
					locationLabel.setVisibility(View.GONE);
					discriptionLabel.setVisibility(View.GONE);
					description.setVisibility(View.GONE);
				}

				return infoWindow;
			}

		});
	}

	protected void toggleBetweenDeleteEditButtons(String markedBy, ImageView deleteButton, ImageView editButton) {
		if ("admin".equals(markedBy)) {
			deleteButton.setVisibility(View.INVISIBLE);
			editButton.setVisibility(View.INVISIBLE);
		}

		if ("user".equals(markedBy)) {
			deleteButton.setVisibility(View.VISIBLE);
			editButton.setVisibility(View.VISIBLE);
		}
	}

	// To Mark location when click on mark here image on info window
	private OnInfoWindowItemTouchListener setOnTouchListenerOnMarkHere(ImageView markHere) {

		final OnInfoWindowItemTouchListener markHereButtonListener = new OnInfoWindowItemTouchListener(markHere, 0, 0) {
			@Override
			protected void onClickConfirmed(View v, Marker marker) {
				longTappedMarkerForSave = marker;
				marker.hideInfoWindow();
				Intent i = new Intent(getApplicationContext(), MarkerSaveFormActivity.class);
				LatLng latLng = marker.getPosition();
				final double lat = latLng.latitude;
				final double lon = latLng.longitude;
				String markedBy = getMarkedBy();
				i.putExtra("latitude", lat);
				i.putExtra("longitude", lon);
				i.putExtra("zoomLevel", currentZoomLevel);
				i.putExtra("MarkedBy", markedBy);
				Log.i("Click Marker on Info window ","Successfully performed click on marker here button on info window");
				startActivityForResult(i, ActivityResult.SAVE_MARKER_RESULT);
			}
		};
		markHere.setOnTouchListener(markHereButtonListener);
		return markHereButtonListener;
	}

	// To Delete existing Marker on map when click on delete image button on
	// info window
	private OnInfoWindowItemTouchListener setOnTouchListenerOnDeleteButton(ImageView deleteButton) {
		final OnInfoWindowItemTouchListener deleteButtonListener = new OnInfoWindowItemTouchListener(deleteButton, 0, 0) {
			@Override
			protected void onClickConfirmed(View v, final Marker marker) {
				final Context context = getBaseContext();
				final MarkingInfo markingInfo = userMarkers.get(marker);

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MapMakerActivity.this);
				// set title
				alertDialogBuilder.setTitle("Are you sure you want to delete" + " " + "\"" + markingInfo.getName()
						+ "\"" + " " + "marker ?");

				// set dialog message
				alertDialogBuilder.setMessage("Click 'Yes' to delete!").setCancelable(false)
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked,delete marker

								// Remove permanently from db
								SQLLiteManager.getInstance(getApplicationContext()).deleteMarkInfo(markingInfo.getId());
								Log.i("Click Delete on Info window","Successfully performed click on delete button on info window");
								Toast.makeText(context, "Marker deleted successfully", Toast.LENGTH_SHORT).show();
								
								// Remove on map 
								marker.remove();
							}
						}).setNegativeButton("No", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// do nothing
								dialog.cancel();
							}
						});
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
				marker.hideInfoWindow();
			}
		};

		deleteButton.setOnTouchListener(deleteButtonListener);
		return deleteButtonListener;
	}

	// To edit existing Marker on map when click on edit image button on info
	// window
	private OnInfoWindowItemTouchListener setOnTouchListenerEditButton(ImageView editButton) {
		final OnInfoWindowItemTouchListener editButtonListener = new OnInfoWindowItemTouchListener(editButton, 0, 0) {
			@Override
			protected void onClickConfirmed(View v, Marker marker) {
				// To move camera to this marker on map after edit
				longTappedMarkerForSave = marker;
				
				String markedBy = getMarkedBy();
				Intent intent = new Intent(v.getContext(), MarkerSaveFormActivity.class);
				intent.putExtra("zoomLevel", currentZoomLevel);
				intent.putExtra("MarkedBy", markedBy);
				intent.putExtra("markingInfo", userMarkers.get(marker));
				((MapMakerActivity)v.getContext()).startActivityForResult(intent, ActivityResult.SAVE_MARKER_RESULT);
				Log.i("Click Edit on Info window ","Successfully performed click on edit button on info window");
				marker.hideInfoWindow();
			}
		};

		editButton.setOnTouchListener(editButtonListener);
		return editButtonListener;
	}

	// To show route on map when click on get me here image button on info
	private OnInfoWindowItemTouchListener setOnTouchListenerOnGetMeHere(final ImageView getMeHere) {

		final OnInfoWindowItemTouchListener getMeHereButtonListener = new OnInfoWindowItemTouchListener(getMeHere, 0, 0) {

			@Override
			protected void onClickConfirmed(View v, final Marker marker) {
				LatLng selectedMarkerPosition = marker.getPosition();

				if (myLatLng == null) {
					Toast.makeText(getApplicationContext(), "Can not get your current location , try after some time.",
							Toast.LENGTH_LONG).show();
				} else {
					marker.hideInfoWindow();
					final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?"
							+ "saddr=" + myLatLng.latitude + "," + myLatLng.longitude + "&daddr="
							+ selectedMarkerPosition.latitude + "," + selectedMarkerPosition.longitude));
					intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
					Log.i("Click Getmehere on Info window ","Successfully performed click on getmehere button on info window");
					startActivity(intent);
				}
			}
		};

		getMeHere.setOnTouchListener(getMeHereButtonListener);
		return getMeHereButtonListener;
	}

	private void listenOnCameraChange() {
		googleMap.setOnCameraChangeListener(new OnCameraChangeListener() {

			@Override
			public void onCameraChange(CameraPosition position) {
				Set<Entry<Marker, MarkingInfo>> entrySet = userMarkers.entrySet();
				currentZoomLevel = position.zoom;
				for (Entry<Marker, MarkingInfo> entry : entrySet) {
					Marker marker = entry.getKey();
					MarkingInfo markingInfo = entry.getValue();

					if (currentZoomLevel >= markingInfo.getZoomlevel()) {
						marker.setVisible(true);
					} else {
						marker.setVisible(false);
					}
					marker.setDraggable(false);
				}
			}
		});
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		int[] to = new int[] { R.id.location_name, R.id.latitude, R.id.longitude, R.id.location_id };
		String[] from = new String[] { "LocationName", "Latitude", "Longitude", "_id" };

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		// To show search widget
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

		// Hide slide out on search click
		searchView.setOnSearchClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//hide slide out menu
				hideSlideOut();
				// hide soft kaypad
				//InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					//imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
			}
		});

		searchView.setQueryHint("Search for location");
		searchView.setOnSuggestionListener(new OnSuggestionListener() {

			@Override
			public boolean onSuggestionSelect(int position) {
				return false;
			}

			@Override
			public boolean onSuggestionClick(int position) {
				// Get lat and long of selected location

				Cursor cursor = searchView.getSuggestionsAdapter().getCursor();
				double lat = cursor.getDouble(4);
				double lon = cursor.getDouble(5);
				int zoomLevel = cursor.getInt(7);

				// Animate to selected location and zoom
				LatLng selectedCoridinate = new LatLng(lat, lon);
				MapUtil.animateMapToPositionAndZoom(googleMap, selectedCoridinate, zoomLevel);

				int markerId = cursor.getInt(0);
				Marker marker = getMarker(markerId);
				// Hide soft window and clear action view collapse
				getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
				searchView.clearFocus();
		        searchView.setFocusable(false);
				
				if (marker != null) {
					MapUtil.showInfoWindow(marker);
					Log.i("Info window", "Show info window when item click on searchview");
					marker.setDraggable(false);
				}

				return true;
			}
		});

		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setIconifiedByDefault(true);
		
		Cursor cursor = null;
		if (SQLLiteManager.getInstance(getApplicationContext()).isTableExists(SQLLite.MARK_INFO_TABLE_NAME)) {
			cursor = SQLLiteManager.getInstance(getApplicationContext()).getLocationNameList();
		}
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.list, cursor, from, to, 0);

		searchView.setSuggestionsAdapter(adapter);
		Log.i("Search view list", "Successfully added marker list into search view");

		adapter.setFilterQueryProvider(new FilterQueryProvider() {
			@Override
			public Cursor runQuery(CharSequence constraint) {
				if (constraint != null && constraint.length() > 0) {
					return SQLLiteManager.getInstance(getApplicationContext()).fetchLocations(constraint.toString());
				}
				return null;

			}
		});

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_search) {
			onSearchRequested();
			return true;
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		if (!AppState.INSTANCE.homeActivityFQN.isEmpty()) {
			Intent i = null;
			try {
				i = new Intent(getApplicationContext(), Class.forName(AppState.INSTANCE.homeActivityFQN));
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			setResult(RESULT_CANCELED);
			finish(); // close current activity first
			startActivity(i);
		} else {
			//super.onBackPressed();
			moveTaskToBack(true);
		}
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			if (viewActionsContentView.isActionsShown()) {
				viewActionsContentView.showContent();
				Log.i("show content", "Successfully show conent when on click on slide out menu item");
			} else {
				viewActionsContentView.showActions();
			}

			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

	// To initialize default map
	private void initGoogleMap() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (googleMap == null) {
			android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
			googleMap = ((SupportMapFragment) fragmentManager.findFragmentById(R.id.map)).getMap();
			Log.i("Google map", "Successfully Googlemap is opened");

			// Check if we were successful in obtaining the map.
			if (googleMap != null) {
				// It shows current location marker
				googleMap.setMyLocationEnabled(true);
				// Add Markers
				addExistingMarkers(Color.BLUE);
			}
		}
	}

	private void setUpLocationClientIfNeeded() {
		createLocReq();
		if (mLocationClient == null) {
			mLocationClient = new LocationClient(getApplicationContext(), this, // ConnectionCallbacks
					this); // OnConnectionFailedListener
		}
	}

	// To show the map with existing user created markers
	private void addExistingMarkers(int color) {
		userMarkers.clear();// remove all existing markers
		/**
		 * Placing Marker
		 * */
		List<MarkingInfo> markingInfos = SQLLiteManager.getInstance(getApplicationContext())
				.getAllAppMarkingInfos();

		for (MarkingInfo markingInfo : markingInfos) {

			// Set title on marker
			//Bitmap drawBmp = writeTextOnDrawable(markingInfo, color);
			Bitmap drawBmp = drawTitleOnMarkerIcon(markingInfo, color);

			
			MarkerOptions markerOptions = new MarkerOptions()
					.position(new LatLng(markingInfo.getLattitude(), markingInfo.getLongtitutde()))
					.title(markingInfo.getName())
					.icon(BitmapDescriptorFactory.fromBitmap(drawBmp))
					.icon(BitmapDescriptorFactory.fromBitmap(drawBmp))
					// .anchor(0.5f, 1)
					.draggable(true)
					.visible(false);

			if (googleMap.getCameraPosition().zoom >= markingInfo.getZoomlevel()) {
				markerOptions.visible(true);
			}

			Marker marker = googleMap.addMarker(markerOptions);
			userMarkers.put(marker, markingInfo);
			marker.setDraggable(false);
		}
		Log.i("Existing markers", "Successfully placed existing markes on map ");
	}

	

	private float convertToPixels(Context context, float nDP) {
		final float conversionScale = context.getResources().getDisplayMetrics().density;
		return (nDP) * conversionScale;
	}

	private float reducePixels(Context context, float nDp) {
		final float conversionScale = context.getResources().getDisplayMetrics().density;
		return nDp/conversionScale;
	}
	
	// To put marker name and icon on the map
	private Bitmap drawTitleOnMarkerIcon(MarkingInfo markingInfo, int color) {
		String title = markingInfo.getName();
		
		int icon = AddLocationTypeIcon.getIcon(markingInfo.getType());
		Bitmap bm = BitmapFactory.decodeResource(getResources(), icon).copy(Bitmap.Config.ARGB_8888, true);
		
		int bmWidth = bm.getWidth();
		int bmHeight = bm.getHeight();
		
		int canvasWidth = bmWidth;
		int canvasHeight = bmHeight;
		if (bmWidth > 90) {
			canvasWidth = (int)convertToPixels(getApplicationContext(), bmWidth - bmWidth/2);
			canvasHeight = (int)convertToPixels(getApplicationContext(), bmHeight - bmHeight/2);
			bm = Bitmap.createScaledBitmap(bm, canvasWidth, canvasHeight, true);
		} else {
			canvasWidth = (int)convertToPixels(getApplicationContext(), 72);
			canvasHeight = (int)convertToPixels(getApplicationContext(), 72);
			bm = Bitmap.createScaledBitmap(bm, canvasWidth, canvasHeight, true);
		}
		
		Paint paint = new Paint();
		paint.setColor(color);
		paint.setAntiAlias(true);
		paint.setFakeBoldText(true);
		paint.setTextSize(convertToPixels(getApplicationContext(), paint.getTextSize()-3));
		
		float canvasCoorHeight = canvasHeight - paint.getTextSize()*2;// first y coordinate to start text from 
		while(title.length() > 0) {
			title = drawTextOnCanvas(paint, bm, title, canvasWidth, canvasCoorHeight);
			canvasCoorHeight = canvasCoorHeight + paint.getTextSize();// next text to draw at 12.5 + height
		}

		return bm;
	}

	private String drawTextOnCanvas(Paint paint, Bitmap bm, String title, int canvasWidth, float canvasHeight) {
		Rect titleBoundary = new Rect();
		paint.getTextBounds(title, 0, title.length(), titleBoundary);
		
		int titleWidth = titleBoundary.width();
		
		String bestFitTitle = "";
		if (titleWidth > canvasWidth) {
			bestFitTitle = getBestFitTitle(canvasWidth, title, paint);
			
			// get width of text
			Rect bestFitTitleBoundary = new Rect();
			paint.getTextBounds(bestFitTitle, 0, bestFitTitle.length(), bestFitTitleBoundary);
			titleWidth = bestFitTitleBoundary.width();
			
			title = title.substring(bestFitTitle.length());
		} else {
			bestFitTitle = title;
			title = "";// Finished drawing text 
		}
		
		//Get x coordinate to draw text from on canvas
		float xCoordinate = getXCoordinate(titleWidth, canvasWidth);
		
		Canvas canvas = new Canvas(bm);
		canvas.drawText(bestFitTitle, xCoordinate, canvasHeight, paint);
		
		return title;
	}

	private String getBestFitTitle(int canvasWidth, String title, Paint paint) {
		String bestFit = "";
		for (int i = 1 ; i < title.length() ;i++) {
			bestFit = title.substring(0, i);
			Rect titleBoundary = new Rect();
			paint.getTextBounds(bestFit, 0, bestFit.length(), titleBoundary);
			if (titleBoundary.width() > canvasWidth) {
				bestFit = title.substring(0, i-1); // one char before is i-1,  best fit text
				break;
			}
		}
		return bestFit;
	}

	private float getXCoordinate(int titleWidth, int canvasWidth) {
		int canvasHalfWidth = canvasWidth/2;
		int titleHalfWidth = titleWidth/2;
		return canvasHalfWidth-titleHalfWidth;
	}

	// To draw polygon when long click on map when map mode is DRAW or else show
	// info window
	private void listenOnMapLongPress() {
		googleMap.setOnMapLongClickListener(new OnMapLongClickListener() {

			public void onMapLongClick(final LatLng latLng) {				
				//Animating to the touched position
				googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
	
				MarkerOptions markerOptions = new MarkerOptions().position(latLng);
				// Placing a marker on the touched position
				Marker marker = googleMap.addMarker(markerOptions);
				marker.showInfoWindow();
				Log.i("Long click Info window", "Successfully shown info window when long click on map");
				marker.setVisible(false);
				marker.setDraggable(false);
			}
		});
	}

	// To hide slide out menu when touch on map
	private void listenOnMapClick() {
		googleMap.setOnMapClickListener(new OnMapClickListener() {
			@Override
			public void onMapClick(LatLng latLng) {
				if (MapMode.DRAW == mapMode) {
					Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng).title(latLng.toString()));
					drawCoordinates.add(latLng); // Add it to collection , for
													// undo and save option
					marker.setVisible(false); // disable marker icon on map
					
					polygonOptions.add(marker.getPosition()).strokeColor(Color.RED);
					polygonOptions.fillColor(Color.TRANSPARENT);
					polygonOptions.visible(true);
					
					if (polygon != null) {
						polygon.remove();
					}
					polygon = googleMap.addPolygon(polygonOptions);
				} else {
					hideSlideOut();
				}
			}
		});
	}

	private void hideSlideOut() {
		if (viewActionsContentView.isActionsShown()) {
			viewActionsContentView.showContent();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		/*
		 * // save the map type so when we change orientation, the map type can
		 * be // restored LatLng cameraLatLng =
		 * googleMap.getCameraPosition().target; float cameraZoom =
		 * googleMap.getCameraPosition().zoom; outState.putDouble("lat",
		 * cameraLatLng.latitude); outState.putDouble("lng",
		 * cameraLatLng.longitude); outState.putFloat("zoom", cameraZoom);
		 */
	}

	@Override
	protected void onStart() {
		super.onStart();
		// Connect the client.
		if (!mLocationClient.isConnected()) {
			mLocationClient.connect();
		}
	}

	/*
	 * Called when the Activity is no longer visible.
	 */
	@Override
	protected void onStop() {
		// If the client is connected
		if (mLocationClient.isConnected()) {
			/*
			 * Remove location updates for a listener. The current Activity is
			 * the listener, so the argument is "this".
			 */
			stopPeriodicUpdates();
		}
		/*
		 * After disconnect() is called, the client is considered "dead".
		 */
		mLocationClient.disconnect();
		super.onStop();
	}

	/**
	 * In response to a request to stop updates, send a request to Location
	 * Services
	 */
	private void stopPeriodicUpdates() {
		mLocationClient.removeLocationUpdates(this);
	}

	/**
	 * In response to a request to start updates, send a request to Location
	 * Services
	 */
	private void startPeriodicUpdates() {
		mLocationClient.requestLocationUpdates(mLocationRequest, this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (LocationServices.servicesConnected(this)) {
			if (!isGPSAvailable()) {
				Toast.makeText(this, "Improve your location by enabling GPS", Toast.LENGTH_LONG).show();
			}
			initGoogleMap();
			setUpLocationClientIfNeeded();
			if (!mLocationClient.isConnected()) {
				mLocationClient.connect();
			}
		}
		
		// Update slide out menu
		viewActionsList.setAdapter(getSlideoutAdapter());
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mLocationClient != null && mLocationClient.isConnected()) {
			mLocationClient.disconnect();
		}
	}

	/**
	 * When the map is not ready the CameraUpdateFactory cannot be used. This
	 * should be called on all entry points that call methods on the Google Maps
	 * API.
	 */
	private boolean isMapAvailable() {
		if (googleMap == null) {
			Toast.makeText(this, "Not able to show map", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	// To get marked by whom
	protected String getMarkedBy() {
		return "user";
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		// TODO Auto-generated method stub
		/*
		 * Google Play services can resolve some errors it detects. If the error
		 * has a resolution, try sending an Intent to start a Google Play
		 * services activity that can resolve error.
		 */
		if (connectionResult.hasResolution()) {
			try {
				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(this, LocationServices.CONNECTION_FAILURE_RESOLUTION_REQUEST);

				/*
				 * Thrown if Google Play services canceled the original
				 * PendingIntent
				 */
			} catch (IntentSender.SendIntentException e) {
				// Log the error
				e.printStackTrace();
			}
		} else {
			// If no resolution is available, display a dialog to the user with
			// the error.
			LocationServices.showErrorDialog(connectionResult.getErrorCode(), this);
		}
	}

	/*
	 * Handle results returned to this Activity by other Activities started with
	 * startActivityForResult(). In particular, the method onConnectionFailed()
	 * in LocationUpdateRemover and LocationUpdateRequester may call
	 * startResolutionForResult() to start an Activity that handles Google Play
	 * services problems. The result of this call returns here, to
	 * onActivityResult.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// Choose what to do based on the request code
		switch (requestCode) {
		// If the request code matches the code sent in onConnectionFailed
		case LocationServices.CONNECTION_FAILURE_RESOLUTION_REQUEST:
		case ActivityResult.SAVE_MARKER_RESULT:
			clearAndReloadMap(mapType);
			if (longTappedMarkerForSave != null) {
				MapUtil.animateMapToPositionAndZoom(googleMap, longTappedMarkerForSave.getPosition(), currentZoomLevel);
				longTappedMarkerForSave = null;
			}
			break;
		case ActivityResult.SEARCH_BY_TYPE_RESULT:
			// Show search location based on type
			if (intent != null) {
				showSearchByTypeMarker(intent);
			}
			break;
		default:
			// Report that this Activity received an unknown requestCode
			Log.d("Make My mApp", "No action defind for activity result");
			break;
		}
	}

	@Override
	public void onConnected(Bundle arg0) {
		startPeriodicUpdates();
		Location location = mLocationClient.getLastLocation();
		if (location != null) {
			if (myLatLng == null) { // Only for first time move camera to user location
				onLocationChanged(location);
				MapUtil.animateMapToPositionAndZoom(googleMap, myLatLng, currentZoomLevel);
			} else {
				onLocationChanged(location);
			}
		}
	}

	@Override
	public void onDisconnected() {
	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			userLatitude = location.getLatitude();
			userLongitude = location.getLongitude();
			myLatLng = new LatLng(userLatitude, userLongitude);
		}
	}
}
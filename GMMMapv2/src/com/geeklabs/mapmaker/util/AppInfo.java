package com.geeklabs.mapmaker.util;

import java.io.Serializable;

public class AppInfo implements Serializable {
	private String appName;
	private String appDescription;
	private String appCode;
	private int appId;
	
	private static final long serialVersionUID = 1L;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppDescription() {
		return appDescription;
	}

	public void setAppDescription(String appDescription) {
		this.appDescription = appDescription;
	}

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}
	
	public String getAppCode() {
		return appCode;
	}
	
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
}

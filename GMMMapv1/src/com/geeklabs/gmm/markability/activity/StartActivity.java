package com.geeklabs.gmm.markability.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.geeklabs.gmm.markability.sqllite.MMSQLiteManager;
import com.geeklabs.gmm.markability.util.AppState;
import com.geeklabs.gmm.markability.util.MarkConstants;

public class StartActivity extends Activity {

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		initializeDataBase();

		installMapMarkingInfoIfRequired();

		setAppIdToAppStatusContext();

		// For end user, when he click back button,take him out of app. 
		AppState.INSTANCE.homeActivityFQN = "";
		
		AppState.INSTANCE.appgenFQN = "";
		
		startIntent();
	}

	private void setAppIdToAppStatusContext() {
		// Get the text file
		InputStream inputStream = null;
		try {
			inputStream = getAssets().open("appInfo.gmm");
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					inputStream));

			SQLiteDatabase sqLiteDatabase = openOrCreateDatabase("GMM",
					MODE_PRIVATE, null);
			String appName = reader.readLine();

			Cursor fetchAppsByName = MMSQLiteManager.fetchAppsByName(
					sqLiteDatabase, appName);
			int appId = fetchAppsByName.getInt(0);
			AppState.INSTANCE.appId = appId;

			sqLiteDatabase.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initializeDataBase() {

		// App Info
		MMSQLiteManager
				.createAppDetailsInfoTableIfRequired(openOrCreateDatabase(
						"GMM", MODE_PRIVATE, null));

		// Location Markings Info
		MMSQLiteManager.createMarkingInfo(openOrCreateDatabase("GMM",
				MODE_PRIVATE, null));

		// Installation info table and set status to false
		MMSQLiteManager
				.createInstallationInfoTableIfRequired(openOrCreateDatabase(
						"GMM", MODE_PRIVATE, null));
	}

	private void startIntent() {
		Intent i = new Intent(getApplicationContext(), MainMapActivity.class);
		startActivity(i);
	}

	public void installMapMarkingInfoIfRequired() {

		SQLiteDatabase sqLiteDatabase = openOrCreateDatabase("GMM",
				MODE_PRIVATE, null);
		Cursor cursor = sqLiteDatabase.query(MarkConstants.INSTALLATION_INFO,
				null, null, null, null, null, null);
		cursor.moveToFirst();

		String status = cursor.getString(0);

		if (!"true".equals(status)) {
			// Update App info
			executeAppInfo();

			// Read gmm.sql and run insert query
			executeMarkingInfo();

			// Update status
			MMSQLiteManager.updateInstallationStatus(
					openOrCreateDatabase("GMM", MODE_PRIVATE, null), "true");
		}
	}

	private void executeAppInfo() {
		// Get the text file
		InputStream inputStream = null;
		try {
			inputStream = getAssets().open("appInfo.gmm");
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					inputStream));

			SQLiteDatabase sqLiteDatabase = openOrCreateDatabase("GMM",
					MODE_PRIVATE, null);
			String appName = reader.readLine();
			String appDesc = reader.readLine();

			MMSQLiteManager
					.saveAppDetailsInfo(sqLiteDatabase, appName, appDesc);

			SQLiteDatabase sqLiteDatabase2 = openOrCreateDatabase("GMM",
					MODE_PRIVATE, null);
			Cursor fetchAppsByName = MMSQLiteManager.fetchAppsByName(
					sqLiteDatabase2, appName);
			int appId = fetchAppsByName.getInt(0);
			AppState.INSTANCE.appId = appId;

			sqLiteDatabase2.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void executeMarkingInfo() {
		// Get the text file
		InputStream inputStream = null;
		try {
			inputStream = getAssets().open("markInfo.gmm");
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					inputStream));
			String line = null;

			SQLiteDatabase sqLiteDatabase = openOrCreateDatabase("GMM",
					MODE_PRIVATE, null);
			while ((line = reader.readLine()) != null) {
				if (line != null) {
					line = line.trim();
					line = line.replace(';', ' ');
					line = line.replace("APP_ID", "" + AppState.INSTANCE.appId);
					sqLiteDatabase.execSQL(line);
				}
			}
			sqLiteDatabase.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

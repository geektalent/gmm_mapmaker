package com.geeklabs.mapmaker.util;


public enum AppState { 
	INSTANCE;
	
	public int appId;
	
	public String appName; 
	
	public String homeActivityFQN = "com.geeklabs.appgenerator.activity.AppGeneratorMainActivity";
	
	public String appgenFQN = "com.geeklabs.appgenerator.activity.AppGenMapMakerActivity";

}

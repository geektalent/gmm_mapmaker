package com.geeklabs.gmm.markability.domain;

import static com.geeklabs.gmm.markability.util.TypeConstants.APARTMENT;
import static com.geeklabs.gmm.markability.util.TypeConstants.*;

import java.util.HashMap;
import java.util.Map;

import com.geeklabs.gmm.R;

public final class AddLocationTypeIcon {
	private AddLocationTypeIcon() {
	}

	private static final Map<String, Integer> icons = new HashMap<String, Integer>();

	static {
		icons.put(APARTMENT, R.drawable.apartment);
		icons.put(AUDITORIUM, R.drawable.auditorium);
		icons.put(ATM, R.drawable.atm);
		icons.put(AC_REPAIR_SERVICE, R.drawable.repair_shop);
		icons.put(ADVERTISING_AGENCY, R.drawable.advertise);
		icons.put(AEROBICS_SCHOOL, R.drawable.school);
		//icons.put(AGRICULTURE_SERVICE, R.drawable.x);
		icons.put(AIR_PORT, R.drawable.airport);
		icons.put(AIRLINE_TICKET_AGENCY, R.drawable.airport);
		icons.put(ANIMATION_SCHOOL, R.drawable.animation);
		icons.put(BABY_STORE, R.drawable.kids_shop);
		icons.put(BADMINTON_COURT, R.drawable.badminton_court);
		icons.put(BAG_REPAIR_SERVICE, R.drawable.repair_shop);
		icons.put(BAKERY, R.drawable.bakery);
		icons.put(BANK, R.drawable.bank);
		icons.put(BAR, R.drawable.bar);
		//icons.put(BARBER_SHOP, R.drawable.barber_shop);
		icons.put(BASE_BALL_COURT, R.drawable.base_ball);
		icons.put(BASKET_BALL_COURT, R.drawable.basket_ball);
		icons.put(BEACH, R.drawable.beach);
		icons.put(BEAUTY_PARLOUR, R.drawable.dance_school);
		icons.put(BLOCK, R.drawable.plaza);
		icons.put(BLOOD_BANK, R.drawable.blood_bank);
		icons.put(BUS_STOP, R.drawable.busstop);
		icons.put(CAFE, R.drawable.cafe);
		icons.put(CALL_CENTER, R.drawable.call_center);
		icons.put(CAMERA_STORE, R.drawable.camera);
		icons.put(CAMP, R.drawable.camp);
		icons.put(CANTEEN, R.drawable.bakery);
		icons.put(CAR_RENTAL, R.drawable.driving_school);
		icons.put(CAR_REPAIR_SERVICE, R.drawable.repair_shop);
		//icons.put(CARPENTER, R.drawable.x);
		icons.put(CATERING, R.drawable.hotel);
		icons.put(CBSE_SCHOOL, R.drawable.school);
		icons.put(CELL_PHONE_STORE, R.drawable.cell_phone);
		icons.put(CHARITY_TRUST, R.drawable.plaza);
		icons.put(CHURCH, R.drawable.church);
		icons.put(CLOTH_STORE, R.drawable.cloth);
		icons.put(CLUB, R.drawable.club);
		icons.put(COACHING_INSTITUTE, R.drawable.coachinginstitute);
		icons.put(COLLEGE, R.drawable.college);
		icons.put(COMPLEX, R.drawable.plaza);
		icons.put(CONSULTANCY, R.drawable.consultancy);
		icons.put(COUNTRY_CLUB, R.drawable.club);
		icons.put(COURIER_OFFICE, R.drawable.corrier);
		icons.put(COURT, R.drawable.court);
		icons.put(CRICKET_GROUND, R.drawable.cricket);
		icons.put(CURRY_POINT, R.drawable.curry_point);
		icons.put(DAM, R.drawable.other);
		icons.put(DANCE_SCHOOL, R.drawable.dance_school);
		icons.put(DEGREE_COLLEGE, R.drawable.college);
		icons.put(DENTAL_CLINIC, R.drawable.dental);
		icons.put(DENTAL_COLLEGE, R.drawable.college);
		icons.put(DHABA, R.drawable.dhaba);
		icons.put(DINING_HALL, R.drawable.dining_hall);
		icons.put(DJ, R.drawable.dj);
		icons.put(DOLL_STORE, R.drawable.doll);
		icons.put(DRIVING_SCHOOL, R.drawable.driving_school);
		icons.put(E_SEVA, R.drawable.e_seva);
		icons.put(EDUCATIONAL_CONSULTANT, R.drawable.consultancy);
		icons.put(ELECTRICAL_STORE, R.drawable.electronics);
		icons.put(ELECTRICITY_BOARD, R.drawable.electronics);
		icons.put(ELECTRONIC_GADGET_SERVICE, R.drawable.electronics);
		icons.put(ELECTRONICS, R.drawable.electronics);
		icons.put(EMPLOYMENT_AGENCY, R.drawable.consultancy);
		icons.put(ENGINEERING_COLLEGE, R.drawable.college);
		icons.put(FASHION_DESIGNER, R.drawable.dance_school);
		icons.put(FAST_FOOD_CENTER, R.drawable.curry_point);
		icons.put(FIRE_STATION, R.drawable.fire_station);
		icons.put(FIRST_AID_SERVICE, R.drawable.first_aidbox);
		icons.put(FIVE_STAR_HOTEL, R.drawable.five_star);
		icons.put(FLORIST, R.drawable.florist);
		icons.put(FOOD_COURT, R.drawable.food_court);
		icons.put(FOOT_WEAR, R.drawable.shoe);
		icons.put(FRIDGE_REPAIR_SERVICE, R.drawable.repair_shop);
		icons.put(FUEL_STATION, R.drawable.petrolbunk);
		icons.put(FUNCTION_HALL, R.drawable.functionhall);
		icons.put(FURNITURE, R.drawable.furnitutre);
		icons.put(GAME_STORE, R.drawable.games_store);
		icons.put(GARDEN, R.drawable.garden);
		icons.put(GAS_STATION, R.drawable.gas_station);
		icons.put(GENERAL_BOOK_STORE, R.drawable.book_store);
		icons.put(GENERAL_STORE, R.drawable.stall);
		icons.put(GIFT_SHOP, R.drawable.gift_shop);
		icons.put(GIRLS_HOSTEL, R.drawable.hostel);
		icons.put(GLASS_REPAIR_STORE, R.drawable.repair_shop);
		icons.put(GOLD_SHOP, R.drawable.gold);
		icons.put(GOLF, R.drawable.golf);
		icons.put(GOVERNMENT_OFFICE, R.drawable.office);
		icons.put(GROUND, R.drawable.ground);
		icons.put(GYM, R.drawable.gym);
		//icons.put(HAIR_SALON, R.drawable.barber_shop);
		icons.put(HARDWARE_STORE, R.drawable.hardware);
		icons.put(HEALTH_CLUB, R.drawable.health_care);
		icons.put(HIGH_SCHOOL, R.drawable.school);
		icons.put(HOCKEY_GROUND, R.drawable.hockey);
		icons.put(HOME, R.drawable.home);
		icons.put(HOSPITAL, R.drawable.hospital);
		icons.put(HOSTEL, R.drawable.hostel);
		icons.put(HOTEL, R.drawable.hotel);
		icons.put(HOUSE, R.drawable.home);
		icons.put(ICE_CREAM_PARLOUR, R.drawable.ice_cream_parlour);
		icons.put(ICSE_SCHOOL, R.drawable.school);
		icons.put(IMAX_THEATRE, R.drawable.theatre);
		icons.put(INCOME_TAX_OFFICE, R.drawable.office);
		icons.put(INDUSTRY, R.drawable.industry);
		icons.put(INSTITUTE, R.drawable.coachinginstitute);
		icons.put(INSURANCE_AGENCY, R.drawable.agency);
		icons.put(INTELLIGENCE_AGENCY, R.drawable.agency);
		icons.put(INTERIOR_DESIGNER, R.drawable.interier);
		icons.put(INTERNETCAFE, R.drawable.internetcafe);
		icons.put(INVITATION_SERVICE, R.drawable.invitation);
		icons.put(JEWELRY_STORE, R.drawable.gold);
		icons.put(JUICE_SHOP, R.drawable.juice);
		icons.put(JUNIOR_COLLEGE, R.drawable.college);
		icons.put(KIDS_STORE, R.drawable.kids_shop);
		icons.put(KINDERGARTEN, R.drawable.sports);
		icons.put(LABORATORY, R.drawable.laboratory);
		icons.put(LADIES_EMPORIUM, R.drawable.dance_school);
		//icons.put(LADIES_TAILOR, R.drawable.t);
		icons.put(LAMINATION_STORE, R.drawable.lamination_store);
		icons.put(LAPTOP_STORE, R.drawable.laptop_store);
		icons.put(LAUNDRY, R.drawable.londry_shop);
		icons.put(LAW_BOOK_STORE, R.drawable.book_store);
		icons.put(LAW_LIBRARY, R.drawable.library);
		icons.put(LAW_SCHOOL, R.drawable.college);
		icons.put(LIBRARY, R.drawable.library);
		icons.put(LOAN_AGENCY, R.drawable.agency);
		icons.put(MAGICIAN, R.drawable.magician);
		icons.put(MARKET, R.drawable.market);
		icons.put(MASJID, R.drawable.masjid);
		icons.put(MEDICAL, R.drawable.medical);
		icons.put(MEDICAL_CENTER, R.drawable.medical);
		icons.put(MEDICAL_CLINIC, R.drawable.medical);
		icons.put(MEDICAL_COLLEGE, R.drawable.college);
		icons.put(MENS_CLOTHING_STORE, R.drawable.cloth);
		icons.put(MENS_HOSTEL, R.drawable.hostel);
		//icons.put(MENS_TAILOR, R.drawable.s);
		icons.put(NEWS_SERVICE, R.drawable.news_sevice);
		icons.put(NON_VEG_RESTAURANT, R.drawable.non_veg);
		icons.put(NOTICE_BOARD, R.drawable.notice);
		icons.put(NOVELTIES_STORE, R.drawable.book_store);
		icons.put(OFFICE, R.drawable.office);
		icons.put(OPEN_UNIVERSITY, R.drawable.college);
		icons.put(OPTICAL_STORE, R.drawable.opticals);
		icons.put(ORCHARD, R.drawable.park);
		icons.put(ORPHANAGE, R.drawable.home);
		icons.put(OTHER, R.drawable.other);
		icons.put(PALACE, R.drawable.other);
		icons.put(PARK, R.drawable.park);
		icons.put(PARKING, R.drawable.parking);
		icons.put(PASSPORT_OFFICE, R.drawable.office);
		icons.put(PET_CARE, R.drawable.pe_care);
		icons.put(PETROL_BUNK, R.drawable.petrolbunk);
		icons.put(PHARMACY, R.drawable.medical);
		icons.put(PHOTO_SHOP, R.drawable.photo_shop);
		icons.put(PHOTO_STUDIO, R.drawable.photo_shop);
		//icons.put(PIZZA_HUT, R.drawable.p);
		icons.put(PLAY_SCHOOL, R.drawable.sports);
		icons.put(PLAZA, R.drawable.plaza);
		icons.put(POLICE_STATION, R.drawable.policestation);
		//icons.put(POST_OFFICE, R.drawable.pz);
		icons.put(POWER_PLANT, R.drawable.power_plant);
		icons.put(PRINTING_STORE, R.drawable.printing);
		icons.put(PUB, R.drawable.pub);
		icons.put(RADIO_STATION, R.drawable.radio_station);
		icons.put(RAILWAY_STATION, R.drawable.railwaystation);
		icons.put(RECEPTION, R.drawable.other);
		icons.put(REPAIR_SERVICE, R.drawable.repair_shop);
		icons.put(RESORT, R.drawable.resort);
		icons.put(REST_ROOM, R.drawable.waitinghall);
		icons.put(RESTAURANT, R.drawable.hotel);
		icons.put(ROOM, R.drawable.home);
		//icons.put(SALON, R.drawable.barber_shop);
		icons.put(SCHOOL, R.drawable.school);
		icons.put(SHOE_STORE, R.drawable.shoe);
		icons.put(SHOPPING_MALL, R.drawable.shoppingmall);
		icons.put(SHUTTLE, R.drawable.badminton_court);
		icons.put(SPORTS, R.drawable.sports);
		icons.put(SPORTS_CLUB, R.drawable.sports);
		icons.put(STALLS, R.drawable.stall);
		icons.put(STORE, R.drawable.store);
		icons.put(SUBSTATION, R.drawable.electronics);
		icons.put(SUPER_MARKET, R.drawable.supermarket);
		icons.put(SWEET_HOUSE, R.drawable.sweethome);
		icons.put(SWIMMING_POOL, R.drawable.swimming_pool);
		//icons.put(TAILOR, R.drawable.s);
		//icons.put(TANK, R.drawable.z);
		icons.put(TAXI_SEVICE, R.drawable.taxi_service);
		icons.put(TEA_STALL, R.drawable.tea_stall);
		icons.put(TELEPHONE_EXCHANGE, R.drawable.telephone_exchange);
		//icons.put(TEMPLE, R.drawable.temple);
		icons.put(TENNIS_COURT, R.drawable.tennis);
		icons.put(TEXTILE_STORE, R.drawable.cloth);
		icons.put(THEATRE, R.drawable.theatre);
		icons.put(TICKET_COUNTER, R.drawable.other);
		icons.put(TOURS_AND_TRAVELS, R.drawable.travels);
		icons.put(TUTION_CENTER, R.drawable.school);
		icons.put(TV_REPAIR_SERVICE, R.drawable.repair_shop);
		icons.put(TV_STATION, R.drawable.tv_station);
		icons.put(TV_TOWER, R.drawable.news_sevice);
		icons.put(UNIVERSITY, R.drawable.college);
		icons.put(VEGETABLE_MARKET, R.drawable.market);
		icons.put(VEGETABLE_STORE, R.drawable.market);
		icons.put(VOLLEY_BALL_COURT, R.drawable.volley_ball);
		icons.put(VOLUNTEER_ORGANISATION, R.drawable.organization);
		icons.put(WASH_ROOM, R.drawable.wash_room);
		icons.put(WATCH_STORE, R.drawable.watch);
		icons.put(WEDDING_SERVICE, R.drawable.wedding);
		icons.put(WELDER, R.drawable.welding_shop);
		icons.put(WINE_STORE, R.drawable.pub);
		icons.put(YOGA_CENTER, R.drawable.yoga);
		icons.put(YOUTH_CLUB, R.drawable.youth_club);
		icons.put(ZOO, R.drawable.zoo);

	}

	public static int getIcon(String typeName) {

		// if type name is null or empty show default icon
		if (typeName == null || typeName.isEmpty()) {
			return icons.get("Other");
		}
		// if type name is not matched with existing type show default icon
		if (icons.get(typeName) == null) {
			return icons.get("Other");
		}
		// if type name is matched with existing type show actual icon
		return icons.get(typeName);
	}
}

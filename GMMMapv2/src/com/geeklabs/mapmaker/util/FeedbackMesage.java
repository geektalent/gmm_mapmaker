package com.geeklabs.mapmaker.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public final class FeedbackMesage {

	private FeedbackMesage() {}
	
	public static void showOkButtonAlertDialog(String title, Context context) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		// set title
		alertDialogBuilder.setTitle("Info");
		alertDialogBuilder.setMessage(title);
		// set dialog message
		alertDialogBuilder.setCancelable(false).setNegativeButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do nothing...
						dialog.cancel();
					}
				});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
	
	public static AlertDialog okButtonAlertDialog(String title, Context context) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		// set title
		alertDialogBuilder.setTitle("Info");
		alertDialogBuilder.setMessage(title);
		// set dialog message
		alertDialogBuilder.setCancelable(false).setNegativeButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do nothing...
						dialog.cancel();
					}
				});
		// create alert dialog
		return alertDialogBuilder.create();
	}
}

package com.geeklabs.mapmaker.sqllite;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;

import com.geeklabs.mapmaker.util.AppInfo;
import com.geeklabs.mapmaker.util.MarkingInfo;
import com.google.android.gms.maps.model.LatLng;

/**
 * Don't change order LocationName - 0,LocationType - 1,Description - 2,Latitude
 * -3 ,Longitude-4 ,MarkedBy-5, ZoomLevel-6
 * 
 * 
 */
public class SQLLite {
	public static final String MARK_INFO_TABLE_NAME = "LocationMarkingInfo";
	public static final String APP_INFO_TABLE_NAME = "AppDetailsInfo";
	public static final String DRAW_INFO_TABLE_NAME = "DrawArea";

	public static String formMarkingInfoStringToInsertQuery(String markingInfoAsColonSeparator, int appId) {
		String[] markingInfo = markingInfoAsColonSeparator.split(":");
		return "INSERT INTO " + SQLLite.MARK_INFO_TABLE_NAME
				+ " (LocationName,LocationType,Description,Latitude,Longitude,MarkedBy,ZoomLevel,AppID) VALUES('"
				+ markingInfo[0] + "','" + markingInfo[1] + "','" + markingInfo[2] + "','" + markingInfo[3] + "','"
				+ markingInfo[4] + "','" + markingInfo[5] + "','" + markingInfo[6] + "','" + appId + "')";
	}

	public static String[] latLngToStringSeparated(String boundaryPosition) {
		return boundaryPosition.split(":");
	}

	public static String latLngToStringSeparated(LatLng latLng) {
		StringBuilder builder = new StringBuilder();
		builder.append(latLng.latitude).append(":").append(latLng.longitude);
		return builder.toString();
	}

	public static String markingInfoToStringSeparated(MarkingInfo markingInfo) {
		StringBuilder builder = new StringBuilder();
		builder.append(markingInfo.getName()).append(":").append(markingInfo.getType()).append(":")
				.append(markingInfo.getDescription()).append(":").append(markingInfo.getLattitude()).append(":")
				.append(markingInfo.getLongtitutde()).append(":").append(markingInfo.getMarkedBy()).append(":")
				.append(markingInfo.getZoomlevel());
		return builder.toString();
	}

	public static AppInfo getAppInfo(Cursor cursor) {
		AppInfo appInfo = new AppInfo();

		boolean isExist = cursor.moveToFirst();
		if (isExist) {
			appInfo.setAppDescription(cursor.getString(3));
			appInfo.setAppCode(cursor.getString(2));
			appInfo.setAppName(cursor.getString(1));
			appInfo.setAppId(cursor.getInt(0));

			// If it returns true, there is another app with same name :(
			boolean moveToNext = cursor.moveToNext();
			if (moveToNext) {
				throw new IllegalStateException("Found two apps with same name");
			}
		}
		cursor.close();
		return appInfo;
	}

	public static List<MarkingInfo> iterateMarkingCursor(Cursor cursor) {
		List<MarkingInfo> markings = new ArrayList<MarkingInfo>();
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			MarkingInfo markingInfo = new MarkingInfo();

			markingInfo.setId(cursor.getInt(0));
			markingInfo.setName(cursor.getString(1));
			markingInfo.setType(cursor.getString(2));
			markingInfo.setDescription(cursor.getString(3));
			markingInfo.setLattitude(cursor.getDouble(4));
			markingInfo.setLongtitutde(cursor.getDouble(5));
			markingInfo.setMarkedBy(cursor.getString(6));
			markingInfo.setZoomlevel(cursor.getInt(7));
			markingInfo.setAppId(cursor.getInt(8));

			markings.add(markingInfo);
			cursor.moveToNext();
		}

		cursor.close();
		return markings;
	}
}

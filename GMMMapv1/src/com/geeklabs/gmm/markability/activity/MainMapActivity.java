package com.geeklabs.gmm.markability.activity;

import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

import com.geeklabs.gmm.R;
import com.geeklabs.gmm.markability.customview.MyCustomMapView;
import com.geeklabs.gmm.markability.domain.AddLocationTypeIcon;
import com.geeklabs.gmm.markability.domain.AddMapOverlayItem;
import com.geeklabs.gmm.markability.sqllite.MMSQLiteManager;
import com.geeklabs.gmm.markability.util.AppState;
import com.geeklabs.gmm.markability.util.MarkConstants;
import com.geeklabs.gmm.markability.util.MarkingInfo;
import com.geeklabs.gmm.navigation.route.RouteOverlay;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class MainMapActivity extends MapActivity {

	private MapView mapView;
	private MyLocationOverlay myLocation;
	private MapController mapController;
	List<Overlay> mapOverlays;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main_map);

		mapView = (MyCustomMapView) findViewById(R.id.map_view);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

		StrictMode.setThreadPolicy(policy);

		mapController = mapView.getController();
		int userZoomLevel = getIntent().getIntExtra("zoomLevel", 0);
		if (userZoomLevel != 0) {
			mapController.setZoom(userZoomLevel);
		} else {
			mapController.setZoom(18);
		}
		mapView.setBuiltInZoomControls(true);

		mapOverlays = mapView.getOverlays();

		final List<Overlay> mapOverlays = mapView.getOverlays();
		mapView.getOverlays().add(new TouchOverlay());

		// Add the MyLocationOverlay
		myLocation = new MyLocationOverlay(this, mapView);
		mapOverlays.add(myLocation);

		myLocation.enableMyLocation();
		myLocation.runOnFirstFix(new Runnable() {
			public void run() {
				mapController.animateTo(myLocation.getMyLocation());
			}
		});

		((MyCustomMapView) mapView).setOnLongpressListener(new MyCustomMapView.OnLongpressListener() {
			public void onLongpress(final MapView view, final GeoPoint longpressLocation) {
				runOnUiThread(new Runnable() {
					public void run() {
						// Insert your long press action here

						AlertDialog alertDialog = new AlertDialog.Builder(MainMapActivity.this).create();
						alertDialog.setButton("Mark Here", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								Intent i = new Intent(getApplicationContext(), MarkFormSavingActivity.class);
								double lat = longpressLocation.getLatitudeE6() / 1E6;
								double lon = longpressLocation.getLongitudeE6() / 1E6;
								i.putExtra("latitude", lat);
								i.putExtra("longitude", lon);
								i.putExtra("zoomLevel", mapView.getZoomLevel());
								// Write your code here to
								// execute after dialog
								// closed
								startActivity(i);
							}
						});
						// Showing Alert Message
						alertDialog.show();
					}
				});
			}
		});

		if (MMSQLiteManager.isTableExists(openOrCreateDatabase("GMM", MODE_PRIVATE, null),
				MarkConstants.MARK_INFO_TABLE_NAME)) {
			/**
			 * Placing Marker
			 * */
			List<MarkingInfo> markingInfos = MMSQLiteManager.getMarkingInfo(openOrCreateDatabase("GMM", MODE_PRIVATE,
					null));

			for (MarkingInfo markingInfo : markingInfos) {
				// draw icon on map
				Drawable drawable = this.getResources().getDrawable(AddLocationTypeIcon.getIcon(markingInfo.getType()));
				AddMapOverlayItem overlay = new AddMapOverlayItem(mapView, markingInfo, drawable, this);
				GeoPoint geoPoint = new GeoPoint((int) (markingInfo.getLattitude() * 1E6),
						(int) (markingInfo.getLongtitutde() * 1E6));
				OverlayItem overlayItem = new OverlayItem(geoPoint, markingInfo.getName(), markingInfo.getDescription());
				overlay.addOverlay(overlayItem);
				mapController.animateTo(geoPoint);
				if (mapView.getZoomLevel() >= markingInfo.getZoomlevel()) {
					mapOverlays.add(overlay);
				}
			}

			if (AppState.INSTANCE.routeOverlay != null) {
				mapOverlays.add(AppState.INSTANCE.routeOverlay);
			}
		}
		mapView.setKeepScreenOn(true);
		mapView.setSaveEnabled(false);
		mapView.invalidate();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_mark) {

			Intent intent = new Intent(this, MarkFormSavingActivity.class);
			intent.putExtra("zoomLevel", mapView.getZoomLevel());

			this.startActivity(intent);

			return true;
		}
		if (item.getItemId() == R.id.action_clear) {
			List<Overlay> overlays = mapView.getOverlays();
			for (Overlay overlay : overlays) {
				if (overlay instanceof RouteOverlay) {
					mapView.getOverlays().remove(overlay);
					break;
				}
			}

			AppState.INSTANCE.routeOverlay = null;
			mapView.invalidate();
			return true;
		}

		return onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		myLocation.enableMyLocation();
	}

	@Override
	protected void onPause() {
		super.onPause();
		myLocation.disableMyLocation();
	}

	@Override
	public void onBackPressed() {
		if (!AppState.INSTANCE.homeActivityFQN.isEmpty()) {
			Intent i = null;
			try {
				i = new Intent(getApplicationContext(), Class.forName(AppState.INSTANCE.homeActivityFQN));
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			startActivity(i);
		} else {

			moveTaskToBack(true);

		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	private class TouchOverlay extends com.google.android.maps.Overlay {
		int lastZoomLevel = -1;

		@Override
		public boolean onTouchEvent(MotionEvent event, MapView mapview) {

			if (mapOverlays != null) {
				List<Overlay> overlays = mapview.getOverlays();
				for (Overlay overlay : overlays) {
					if (overlay instanceof AddMapOverlayItem) {
						AddMapOverlayItem addMapOverlayItem = (AddMapOverlayItem) overlay;
						addMapOverlayItem.hideAllBalloons();
						break;
					}
				}
			}
			if (lastZoomLevel == -1) {
				lastZoomLevel = mapView.getZoomLevel();
			}

			if (mapView.getZoomLevel() != lastZoomLevel) {
				onZoom(mapView.getZoomLevel());
				lastZoomLevel = mapView.getZoomLevel();
			}
			return false;
		}
	}

	public void onZoom(int level) {
		Intent intent = new Intent(getApplicationContext(), MainMapActivity.class);
		intent.putExtra("zoomLevel", level);
		startActivity(intent);
	}
}

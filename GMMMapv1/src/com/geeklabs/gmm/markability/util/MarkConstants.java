package com.geeklabs.gmm.markability.util;

public final class MarkConstants {

	private MarkConstants() {}
	
	public static final String MARK_INFO_TABLE_NAME = "LocationMarkingInfo";
	public static final String APP_INFO_TABLE_NAME = "AppDetailsInfo";
	public static final String INSTALLATION_INFO = "Installation_Info_Status";
}

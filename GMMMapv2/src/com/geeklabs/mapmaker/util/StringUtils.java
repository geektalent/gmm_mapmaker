package com.geeklabs.mapmaker.util;

import android.util.Log;

public class StringUtils {

	public static String trim(String input) {		
		if (input != null && !input.isEmpty()) {
			return input.trim();
		}
		return input;
	}
}

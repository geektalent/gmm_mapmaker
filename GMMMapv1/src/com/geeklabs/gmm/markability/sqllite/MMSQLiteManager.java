package com.geeklabs.gmm.markability.sqllite;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.geeklabs.gmm.markability.util.AppState;
import com.geeklabs.gmm.markability.util.MarkConstants;
import com.geeklabs.gmm.markability.util.MarkingInfo;

public class MMSQLiteManager {

	public static boolean isTableExists(SQLiteDatabase db, String tableName) {
		if (tableName == null || db == null || !db.isOpen()) {
			return false;
		}
		Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ? ", new String[] {
				"table", tableName });
		if (!cursor.moveToFirst()) {
			return false;
		}
		int count = cursor.getInt(0);
		cursor.close();
		return count > 0;
	}

	public static void saveMarkingInfo(SQLiteDatabase sqLiteDatabase, double actualLatitude, double actualLongitude,
			String locationName, String locationType, String description,int zoomLevel) {

		if (!isTableExists(sqLiteDatabase, MarkConstants.APP_INFO_TABLE_NAME)) {
			throw new IllegalStateException("App info table doesn't exist");
		}

		int appId = AppState.INSTANCE.appId;
		sqLiteDatabase
				.execSQL("CREATE TABLE IF NOT EXISTS "
						+ MarkConstants.MARK_INFO_TABLE_NAME
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, LocationName TEXT NOT NULL,LocationType TEXT NOT NULL,"
						+ " Description TEXT,Latitude DOUBLE NOT NULL,Longitude DOUBLE NOT NULL,ZoomLevel INTEGER NOT NULL, AppID INTEGER NOT NULL, FOREIGN KEY(AppID) REFERENCES "
						+ MarkConstants.APP_INFO_TABLE_NAME + "(Id))");
		sqLiteDatabase.execSQL("INSERT INTO " + MarkConstants.MARK_INFO_TABLE_NAME
				+ " (LocationName,LocationType,Description,Latitude,Longitude,ZoomLevel,AppID) VALUES('" + locationName + "','"
				+ locationType + "','" + description + "','" + actualLatitude + "','" + actualLongitude + "','"+ zoomLevel+"','" + appId
				+ "')");

		sqLiteDatabase.close();
	}

	public static void createMarkingInfo(SQLiteDatabase sqLiteDatabase) {
		sqLiteDatabase
				.execSQL("CREATE TABLE IF NOT EXISTS "
						+ MarkConstants.MARK_INFO_TABLE_NAME
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, LocationName TEXT NOT NULL,LocationType TEXT NOT NULL,"
						+ " Description TEXT,Latitude DOUBLE NOT NULL,Longitude DOUBLE NOT NULL,ZoomLevel INTEGER NOT NULL,AppID INTEGER NOT NULL, FOREIGN KEY(AppID) REFERENCES "
						+ MarkConstants.APP_INFO_TABLE_NAME + "(Id))");

		sqLiteDatabase.close();
	}

	public static List<MarkingInfo> getMarkingInfo(SQLiteDatabase sqLiteDatabase) {

		List<MarkingInfo> markings = new ArrayList<MarkingInfo>();

		// Write query to get details from Database;

		int appId = AppState.INSTANCE.appId;

		// select * from marking where AppId = '"+appId+"'
		Cursor cursor = sqLiteDatabase.query(MarkConstants.MARK_INFO_TABLE_NAME, null, "AppID=?", new String[] { ""
				+ appId }, null, null, null);
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			MarkingInfo markingInfo = new MarkingInfo();

			markingInfo.setId(cursor.getInt(0));
			markingInfo.setName(cursor.getString(1));
			markingInfo.setType(cursor.getString(2));
			markingInfo.setDescription(cursor.getString(3));
			markingInfo.setLattitude(cursor.getDouble(4));
			markingInfo.setLongtitutde(cursor.getDouble(5));
			markingInfo.setZoomlevel(cursor.getInt(6));
			markingInfo.setAppId(cursor.getInt(7));
			markings.add(markingInfo);
			cursor.moveToNext();
		}

		return markings;
	}

	public static void createAppDetailsInfoTableIfRequired(SQLiteDatabase sqLiteDatabase) {

		sqLiteDatabase
				.execSQL("CREATE TABLE IF NOT EXISTS "
						+ MarkConstants.APP_INFO_TABLE_NAME
						+ " (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ApplicatioName TEXT NOT NULL,Description TEXT NOT NULL)");

		sqLiteDatabase.close();
	}

	public static void saveAppDetailsInfoIfNotExist(SQLiteDatabase sqLiteDatabase, String applicationname,
			String description) {
		Cursor cursor = sqLiteDatabase.query(MarkConstants.APP_INFO_TABLE_NAME, null, null, null, null, null, null);
		if (!cursor.moveToFirst()) {
			saveAppDetailsInfo(sqLiteDatabase, applicationname, description);
		}
	}

	public static void saveAppDetailsInfo(SQLiteDatabase sqLiteDatabase, String applicationname, String description) {
		sqLiteDatabase.execSQL("INSERT INTO " + MarkConstants.APP_INFO_TABLE_NAME
				+ " (ApplicatioName,Description) VALUES('" + applicationname + "','" + description + "')");

		sqLiteDatabase.close();

	}

	public static Cursor getList(SQLiteDatabase sqLiteDatabase) {

		//List<String> listInfoChange = new ArrayList<String>();

		// Write query to get details from Database;
		Cursor cursor = sqLiteDatabase.query(MarkConstants.APP_INFO_TABLE_NAME, null, null, null, null, null, null);
		/*
		 * cursor.moveToFirst(); while (cursor.isAfterLast() == false) {
		 * listInfoChange.add(cursor.getString(1)); cursor.moveToNext(); }
		 * 
		 * return listInfoChange;
		 */
		return cursor;
	}

	public static Cursor fetchAppsByName(SQLiteDatabase sqLiteDatabase, String appNameStartsWith) {
		Cursor cursor = sqLiteDatabase.query(MarkConstants.APP_INFO_TABLE_NAME, null, " ApplicatioName like '%"
				+ appNameStartsWith + "%' ", null, null, null, null);
		cursor.moveToFirst();

		return cursor;
	}

	public static Cursor fetchAppById(SQLiteDatabase sqLiteDatabase, String appId) {
		Cursor cursor = sqLiteDatabase.query(MarkConstants.APP_INFO_TABLE_NAME, null, " _id=?", new String[] { ""
				+ appId } , null, null, null);
		cursor.moveToFirst();

		return cursor;
	}
	
	public static void createInstallationInfoTableIfRequired(SQLiteDatabase sqLiteDatabase) {

		sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + MarkConstants.INSTALLATION_INFO
				+ " (status TEXT NOT NULL)");
		Cursor cursor = sqLiteDatabase.query(MarkConstants.INSTALLATION_INFO, null, null, null, null, null, null);
		if (!cursor.moveToFirst()) {
			sqLiteDatabase.execSQL("INSERT INTO " + MarkConstants.INSTALLATION_INFO + " (status) VALUES('false')");
		}
		sqLiteDatabase.close();
	}

	public static void updateInstallationStatus(SQLiteDatabase sqLiteDatabase, String status) {
		sqLiteDatabase.execSQL("UPDATE " + MarkConstants.INSTALLATION_INFO + " set status='" + status + "'");

		sqLiteDatabase.close();

	}
	
	public static void deleteMarkInfo(SQLiteDatabase sqLiteDatabase, int id) {
		
		sqLiteDatabase.execSQL("DELETE FROM " + MarkConstants.MARK_INFO_TABLE_NAME + " WHERE Id = '" + id + "'");
		sqLiteDatabase.close();
		
	}

	public static void updateMarkingInfo(SQLiteDatabase sqLiteDatabase, String locationname, String locationtype,
			String description, int zoomLevel, int id) {
		sqLiteDatabase.execSQL("UPDATE " + MarkConstants.MARK_INFO_TABLE_NAME  + " set LocationName='" +locationname+ "'," + " LocationType='"+locationtype+ "'," + " Description='" +description+ "' where Id='" +id+ "'");

		sqLiteDatabase.close();
		
	}

	public static void clearMarkings(SQLiteDatabase sqLiteDatabase, int id) {
		
		sqLiteDatabase.execSQL("DELETE FROM " + MarkConstants.MARK_INFO_TABLE_NAME + " WHERE AppID = '" + id + "'");
		sqLiteDatabase.close();
		
	}
}

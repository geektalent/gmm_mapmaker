package com.geeklabs.mapmaker.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

import com.geeklabs.mapmaker.sqllite.SQLLite;
import com.geeklabs.mapmaker.sqllite.SQLLiteManager;
import com.geeklabs.mapmaker.util.AppInfo;
import com.geeklabs.mapmaker.util.AppState;
import com.google.android.gms.maps.model.LatLng;

public class StartActivity extends Activity {

	@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
	} 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		initializeDataBase();
		
		boolean isAppAlreadyInstalled = executeAndGetAppUpdate();
		if (!isAppAlreadyInstalled) {
			insertMarkingInfo();
			insertDrawArea();
		} else {
			updateMarkingInfo();
			updateDrawArea();
		}

		// For end user, when he click back button,take him out of app.
		AppState.INSTANCE.homeActivityFQN = "";

		AppState.INSTANCE.appgenFQN = "";

		startMapMainIntent();
	}	

	private boolean executeAndGetAppUpdate() {
		// Get the text file
		boolean isAppAlreadyInstalled = false;
		InputStream inputStream = null;
		try {
			inputStream = getAssets().open("appInfo.gmm");
		} catch (IOException e1) {
			e1.printStackTrace();
			Log.e("App Table", "Problem occured while save/update app table " + e1.getMessage());
			throw new IllegalStateException("Problem occured while save/update app table " + e1.getMessage());
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

			// Get app details
			String[] appInfo = reader.readLine().split(":");
			
			String appName = appInfo[0];
			String appCode = appInfo[1];
			String appDesc = "";
			if (appInfo.length == 3) {			
				appDesc = appInfo[2];
			}

			// is app exist already
			isAppAlreadyInstalled = SQLLiteManager.getInstance(getApplicationContext()).isAppByCodeExist(appCode);
			Log.i("App update", "Application code " + appCode + " is upgrade triggered");
			
			if (!isAppAlreadyInstalled) { // Insert app
				SQLLiteManager.getInstance(getApplicationContext()).saveAppDetailsInfo(appName, appDesc, appCode);				
			} else { // Update app
				AppInfo appByCode = SQLLiteManager.getInstance(getApplicationContext()).fetchAppByCode(appCode);
				SQLLiteManager.getInstance(getApplicationContext()).updateAppDetailsInfo(appName, appDesc, appByCode.getAppId());
			}

			AppInfo appByCode = SQLLiteManager.getInstance(getApplicationContext()).fetchAppByCode(appCode);
			
			// Set App id 
			AppState.INSTANCE.appId = appByCode.getAppId();
			
			Log.i("App Table",  "Successfully upated app "+appName);
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
			Log.e("App Table", "Problem occured while save/update app table " + e.getMessage());
			throw new IllegalStateException("Problem occured while save/update app table " + e.getMessage());
		}
		return isAppAlreadyInstalled;
	}

	private void initializeDataBase() {
		// getInstance method will try to create db. Once db has been created
		// 'onCreate()' in SQLLiteManager class will be called, where we create
		// db initialize
		SQLLiteManager.getInstance(getApplicationContext());
		//sqlLiteManager.close();
	}
	// To get map main intent
	private void startMapMainIntent() {
		Intent i = new Intent(getApplicationContext(), MapMakerActivity.class);
		Log.i("Map main intent", "Successfully started map main intent");
		startActivity(i);
	}
    // To insert existing draw area
	private void insertDrawArea() {
		// Get the text file
		InputStream inputStream = null;
		try {
			inputStream = getAssets().open("boundary.gmm");
		} catch (IOException e1) {
			e1.printStackTrace();
			Log.e("Draw boundary Table", "Problem occured while save/update Draw boundary table " + e1.getMessage());
			throw new IllegalStateException("Problem occured while save/update Draw boundary table " + e1.getMessage());
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;
			LinkedList<LatLng> drawCoordinates = new LinkedList<LatLng>();
			while ((line = reader.readLine()) != null) {
				if (line != null) {
					line = line.trim();
					if (!line.isEmpty()) {
						String[] boundaryPosition = SQLLite.latLngToStringSeparated(line);
						if (boundaryPosition.length != 2) {
							throw new IllegalStateException("Wrongly constructed draw area string seperated line");
						}
						drawCoordinates.add(new LatLng(Double.parseDouble(boundaryPosition[0]), Double.parseDouble(boundaryPosition[1])));
					}
				}
			}
			reader.close();
			if (!drawCoordinates.isEmpty()) {
				SQLLiteManager.getInstance(getApplicationContext()).saveDrawArea(drawCoordinates);
			}

		} catch (IOException e) {
			e.printStackTrace();
			Log.e("Draw boundary Table", "Problem occured while save/update Draw boundary table " + e.getMessage());
			throw new IllegalStateException("Problem occured while save/update Draw boundary table " + e.getMessage());
		}
	}
	// To update draw area
	private void updateDrawArea() {
		
		// first draw area
		SQLLiteManager.getInstance(getApplicationContext()).deleteDrawArea();
		
		// now insert all boundary positions
		insertDrawArea();
	}
	// To update marking info
	private void updateMarkingInfo() {
		// Get the text file
		InputStream inputStream = null;
		try {
			inputStream = getAssets().open("markInfo.gmm");
		} catch (IOException e) {
			e.printStackTrace();
			Log.e("Marking info Table", "Problem occured while save/update Marking table " + e.getMessage());
			throw new IllegalStateException("Problem occured while save/update Marking table " + e.getMessage());
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;
			int appId = AppState.INSTANCE.appId;
			
			while ((line = reader.readLine()) != null) {
				if (line != null) {
					line = line.trim();
					String[] markingInfo = line.split(":");
					int markerId = SQLLiteManager.getInstance(getApplicationContext()).getMarkerId(markingInfo[3], markingInfo[4]);
					if (markerId > 0) {
						float zoomLevel = Float.parseFloat(markingInfo[6]);
						SQLLiteManager.getInstance(getApplicationContext()).updateMarkingInfo(
								markingInfo[0], markingInfo[1], markingInfo[2], zoomLevel, markerId);
					} else {
						String query = SQLLite.formMarkingInfoStringToInsertQuery(line, appId);
						SQLLiteManager.getInstance(getApplicationContext()).executeQuery(query);
					}
				}
			}
			reader.close();
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			Log.e("Marking info Table", "Problem occured while save/update Marking table " + e.getMessage());
			throw new IllegalStateException("Problem occured while save/update Marking table " + e.getMessage());
		}
	}

	// To insert marking info
	private void insertMarkingInfo() {
		// Get the text file
		InputStream inputStream = null;
		try {
			inputStream = getAssets().open("markInfo.gmm");
		} catch (IOException e) {
			e.printStackTrace();
			Log.e("Marking info Table", "Problem occured while save/update Marking table " + e.getMessage());
			throw new IllegalStateException("Problem occured while save/update Marking table " + e.getMessage());
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;
			int appId = AppState.INSTANCE.appId;
			
			while ((line = reader.readLine()) != null) {
				if (line != null) {
					line = line.trim();
					String query = SQLLite.formMarkingInfoStringToInsertQuery(line, appId);
					SQLLiteManager.getInstance(getApplicationContext()).executeQuery(query);	
				}
			}
			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
			Log.e("Marking info Table", "Problem occured while save/update Marking table " + e.getMessage());
			throw new IllegalStateException("Problem occured while save/update Marking table " + e.getMessage());
		}
	}
	
	@Override
	public void onBackPressed() {
		moveTaskToBack(true);
	}
}

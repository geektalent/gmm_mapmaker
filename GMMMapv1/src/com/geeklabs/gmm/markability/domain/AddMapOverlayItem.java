package com.geeklabs.gmm.markability.domain;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.widget.Toast;

import com.geeklabs.gmm.markability.popup.BalloonItemizedOverlay;
import com.geeklabs.gmm.markability.util.MarkingInfo;
import com.geeklabs.gmm.navigation.route.RouteOverlay;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

public class AddMapOverlayItem extends BalloonItemizedOverlay<OverlayItem> {

	private ArrayList<OverlayItem> mapOverlays = new ArrayList<OverlayItem>();

	private Context context;

	private static final int FONT_SIZE = 12;
	private static final int TITLE_MARGIN = 8;
	private int markerHeight;
	RouteOverlay routeOverlay;

	private MarkingInfo markingInfo2;


	public AddMapOverlayItem(Drawable defaultMarker,MapView mapView) {
		super(boundCenter(defaultMarker),mapView);
		markerHeight = ((BitmapDrawable) defaultMarker).getBitmap().getHeight();
	}

	public AddMapOverlayItem(MapView mapView, MarkingInfo markingInfo, Drawable defaultMarker, Context context) {
		this(defaultMarker, mapView);
		markingInfo2 = markingInfo;
		this.context = context;
	}

	@Override
	protected boolean onBalloonTap(int index, OverlayItem item) {
		Toast.makeText(context, "onBalloonTap for overlay index " + index,
				Toast.LENGTH_LONG).show();
		return true;
	}
	
	@Override
	protected OverlayItem createItem(int i) {
		return mapOverlays.get(i);
	}

	@Override
	public int size() {
		return mapOverlays.size();
	}

	@Override
	protected OverlayItem getOverLayItem(int index) {
		return mapOverlays.get(index);
	}

	@Override
	public void draw(android.graphics.Canvas canvas, MapView mapView, boolean shadow) {
		// remove shadow of icons
		if (!shadow) {
			super.draw(canvas, mapView, shadow);

		}

		// go through all OverlayItems and draw title for each of them
		for (OverlayItem item : mapOverlays) {
			/*
			 * Converts latitude & longitude of this overlay item to coordinates
			 * on screen. As we have called boundCenterBottom() in constructor,
			 * so these coordinates will be of the bottom center position of the
			 * displayed marker.
			 */
			GeoPoint point = item.getPoint();
			Point markerBottomCenterCoords = new Point();
			mapView.getProjection().toPixels(point, markerBottomCenterCoords);

			/* Find the width and height of the title */
			TextPaint paintText = new TextPaint();
			Paint paintRect = new Paint();

			Rect rect = new Rect();
			paintText.setTextSize(FONT_SIZE);
			paintText.getTextBounds(item.getTitle(), 0, item.getTitle().length(), rect);

			rect.inset(-TITLE_MARGIN, -TITLE_MARGIN);
			rect.offsetTo(markerBottomCenterCoords.x - rect.width() / 2, markerBottomCenterCoords.y - markerHeight
					- rect.height());

			paintText.setTextAlign(Align.CENTER.RIGHT);
			paintText.setTextSize(FONT_SIZE);
			paintText.setARGB(255, 0, 0, 0);
			paintRect.setARGB(0, 0, 0, 0);

			canvas.drawRoundRect(new RectF(rect), 2, 2, paintRect);
			canvas.drawText(item.getTitle(), rect.left + rect.width() / 2, rect.bottom - TITLE_MARGIN, paintText);
		}
	}

	public void addOverlay(OverlayItem overlay) {
		mapOverlays.add(overlay);
		this.populate();
	}

	@Override
	protected MarkingInfo getMarkingInfoId(int currentFocusedIndex2) {
		return markingInfo2;
	}

	/**
	 * Getting Latitude and Longitude on Touch event
	 * **/

	/*
	 * @Override public boolean onTouchEvent(MotionEvent event, MapView mapView)
	 * {
	 * 
	 * if (event.getAction() == 1) { GeoPoint geopoint
	 * =mapView.getProjection().fromPixels( (int) event.getX(), (int)
	 * event.getY()); double lat = geopoint.getLatitudeE6() / 1E6; double lon =
	 * geopoint.getLongitudeE6() / 1E6; Toast.makeText(context, "Lat: " + lat +
	 * ", Lon: "+lon, Toast.LENGTH_LONG).show(); } return false; }
	 */

}
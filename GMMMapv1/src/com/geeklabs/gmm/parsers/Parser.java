package com.geeklabs.gmm.parsers;

import com.geeklabs.gmm.navigation.route.Route;

public interface Parser {
	public Route parse();

}

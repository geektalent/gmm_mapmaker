package com.geeklabs.mapmaker.util;

import java.io.Serializable;

import com.google.android.gms.maps.model.LatLng;

public class MarkingInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String type;
	private String description;
	private double longitude;
	private double latitude;
	private int id;
	private int zoomlevel;
	private String markedBy;

	public void setMarkedBy(String markedBy) {
		this.markedBy = markedBy;
	}

	public String getMarkedBy() {
		return markedBy;
	}

	private int appId;

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getLongtitutde() {
		return longitude;
	}

	public void setLongtitutde(double longtitutde) {
		this.longitude = longtitutde;
	}

	public double getLattitude() {
		return latitude;
	}

	public void setLattitude(double lattitude) {
		this.latitude = lattitude;
	}

	public int getZoomlevel() {
		return zoomlevel;
	}

	public void setZoomlevel(int zoomlevel) {
		this.zoomlevel = zoomlevel;
	}

	public LatLng getPosition() {
		if (latitude != 0.0 && longitude != 0.0) {
			return new LatLng(latitude, longitude);			
		}
		throw new IllegalStateException("Marking info doesn't have latitude and longitude");
	}
}

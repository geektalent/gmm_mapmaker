package com.geeklabs.gmm.markability.util;

import com.geeklabs.gmm.navigation.route.RouteOverlay;

public enum AppState { 
	INSTANCE;
	public RouteOverlay routeOverlay;
	
	public int appId;
	
	public String homeActivityFQN = "com.geeklabs.appgenerartor.activity.AppGeneratorMainActivity";
	
	public String appgenFQN = "com.geeklabs.appgenerartor.activity.AppGenMapViewActivity";

}

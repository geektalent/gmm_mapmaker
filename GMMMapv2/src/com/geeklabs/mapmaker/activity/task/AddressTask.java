package com.geeklabs.mapmaker.activity.task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

public class AddressTask extends AsyncTask<LatLng, Void, String> {
	Activity mContext;
	private TextView addressTextView;

    public AddressTask(Activity context, TextView address) {
        super();
        mContext = context;
		this.addressTextView = address;
    }

    @Override
    protected String doInBackground(LatLng... params) {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

        LatLng loc = params[0];
        List<Address> addresses = null;
        try {
            // Call the synchronous getFromLocation() method by passing in the lat/long values.
            addresses = geocoder.getFromLocation(loc.latitude, loc.longitude, 1);
            Log.i("LatLng", "Address is successfully get from latlng");
        } catch (IOException e) {
        	
            e.printStackTrace();
            Log.e("LatLng", "Unable to get latlng from address"+e.getMessage());
            // Update UI field with the exception.
           // Message.obtain(mHandler, UPDATE_ADDRESS, e.toString()).sendToTarget();
           // updateAddress("Not available");
            
            mContext.runOnUiThread(new Runnable() {
                @Override
                    public void run() {
                	addressTextView.setText("Not available");        
                }  });
        }
        String addressText = "";
        if (addresses != null && addresses.size() > 0) {
            Address address = addresses.get(0);
            // Format the first line of address (if available), city
             addressText = String.format("%s, %s",
                    address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                    address.getLocality());
            // Update the UI via a message handler.
           // Message.obtain(mHandler, UPDATE_ADDRESS, addressText).sendToTarget();
           // addressTextView.setText(addressText);
           // updateAddress(addressText);
            
           /* mContext.runOnUiThread(new Runnable() {
                @Override
                    public void run() {
                	addressTextView.setText(addressText);        
                }  });*/
        }
        return addressText;
    }

	protected void updateAddress(String addressText) {
		
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		addressTextView.setText(result);     
	}
}

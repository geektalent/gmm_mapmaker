package com.geeklabs.mapmaker.util;

import static com.geeklabs.mapmaker.util.TypeConstants.*;

import java.util.Arrays;
import java.util.List;

import android.util.Log;

public final class MarkConstants {

	private MarkConstants() {
	}

	public static final String LOCATION_TYPES[] = { AC_REPAIR_SERVICE, ADVERTISING_AGENCY,
			ADVERTISING_DEPARTMENT, ADVOCATE, AEROBICS_INSTRUCTOR, AEROBICS_SCHOOL, AGRICULTURE_SERVICE,
			AGRICULTURE_STORE, AIR_PORT, AIRLINE_TICKET_AGENCY, ANIMATION_SCHOOL, APARTMENT, ATM, AUDIT, AUDITORIUM,
			BABY_STORE, BADMINTON_COURT, BAG_REPAIR_SERVICE, BAKERY, BANK, BANQUET_HALL, BAR, BARBER_SHOP,
			BASE_BALL_COURT, BASKET_BALL_COURT, BATTERY_STORE, BEACH, BEAUTY_PARLOUR, BELT_SHOP, BICYCLE_STORE,
			BIRD_SHOP, BLOCK, BLOOD_BANK, BOOK_STALL, BRANCH, BUILDING, BUS_DEPOT, BUS_STATION, BUS_STOP, CABINET_STORE,
			CAFE, CALL_CENTER, CAMERA_STORE, CAMP, CAMP_GROUND, CANTEEN, CAR_DEALER, CAR_RENTAL, CAR_REPAIR,
			CAR_REPAIR_SERVICE, CAR_WASH, CARPENTER, CATERING, CAFETERIA, CBSE_SCHOOL, CELL_PHONE_STORE,CELL_TOWER, CHARITY_TRUST,
			CHEMICAL_PLANT, CHURCH, CLOTH_STORE, CLUB, COACHING_INSTITUTE, COLLEGE, COMPLEX, CONSULTANCY, COUNTRY_CLUB,
			COURIER_OFFICE, COURT, CRICKET_GROUND, CURRY_POINT, DAIRY_FARM, DAM, DANCE_SCHOOL, DAY_CARE_CENTER,
			DEAF_SCHOOL, DEGREE_COLLEGE, DENTAL_CLINIC, DENTAL_COLLEGE, DENTAL_COLLEGE, DEPARTMENT, DHABA, DHARMASHALA,
			DINING_HALL, DJ, DOLL_STORE, DRIVING_SCHOOL, E_SEVA, EDUCATIONAL_CONSULTANT, EGG_SUPPLIER,
			ELECTRIC_WIRES_CABLES_STORE, ELECTRICAL_STORE, ELECTRICITY_BOARD, ELECTRONIC_GADGET_SERVICE, ELECTRONICS,
			EMERGENCY_ROOM, EMPLOYMENT_AGENCY, ENDOSCOPIST, ENGINEERING_COLLEGE, ENQUIRY, FASHION_DESIGNER,
			FAST_FOOD_CENTER, FINANCE, FIRE_PROTECTION, FIRE_STATION, FIRST_AID_SERVICE, FIVE_STAR_HOTEL, FLORIST,
			FOOD_COURT, FOOT_WEAR, FRIDGE_REPAIR_SERVICE, FUEL_STATION, FUNCTION_HALL, FURNITURE, GAME_STORE, GARDEN,
			GAS_STATION, GENERAL_BOOK_STORE, GENERAL_STORE, GIFT_SHOP, GIRLS_HOSTEL, GLASS_REPAIR_STORE, GOLD_SHOP,
			GOLF, GOLF_CLUB, GOVERNMENT_HOSPITAL, GOVERNMENT_OFFICE, GROUND, GYM, HAIR_SALON, HANDBALL_COURT,
			HARDWARE_STORE, HEALTH_CLUB, HEART_HOSPITAL, HIGH_SCHOOL, HINDU_TEMPLE, HOCKEY_GROUND, HOME,
			HOME_GOODS_STORE, HOSPITAL, HOSTEL, HOTEL, HOUSE, ICE_CREAM_PARLOUR, ICSE_SCHOOL, IMAX_THEATRE,
			INCOME_TAX_OFFICE, INDUSTRY, INSTITUTE, INSURANCE_AGENCY, INTELLIGENCE_AGENCY, INTERIOR_DESIGNER,
			INTERNATIONAL_SCHOOL, INTERNETCAFE, INVITATION_SERVICE, JEWELRY_STORE, JUICE_SHOP, JUICE_SHOP,
			JUNIOR_COLLEGE, KABADDI_CLUB, KARATE_SCHOOL, KIDS_STORE, KINDERGARTEN, KITE_SHOP,
			LABOR_EMPLOYMENT_CONSULTANT, LABORATORY, LABOUR_UNION, LADIES_EMPORIUM, LADIES_TAILOR, LAMINATION_STORE,
			LAPTOP_STORE, LAUNDRY, LAW_BOOK_STORE, LAW_LIBRARY, LAW_SCHOOL, LIBRARY, LIFE_INSURANCE_AGENCY,
			LIGHT_HOUSE, LOAN_AGENCY, MAGICIAN, MARBLE_STORE, MARKET, MASJID, MEDICAL, MEDICAL_CENTER, MEDICAL_CLINIC,
			MEDICAL_COLLEGE, MENS_CLOTHING_STORE, MENS_HOSTEL, MENS_TAILOR, MESS, MOBILE_STORE, NATIONAL_PARK,
			NEWS_PAPER, NEWS_PAPER_PUBLISHER, NEWS_SERVICE, NON_VEG_RESTAURANT, NOODLE_SHOP, NOTICE_BOARD,
			NOVELTIES_STORE, OFFICE, OPEN_UNIVERSITY, OPTICAL_STORE, ORCHARD, ORPHANAGE, OTHER,
			OXYGEN_EQUIPMENT_SUPPLIER, PAINTER, PAINTING, PALACE, PAPER_DISTRIBUTOR, PARK, PARKING, PASSPORT_OFFICE,
			PET_CARE, PETROL_BUNK, PHARMACY, PHOTO_SHOP, PHOTO_STUDIO, PHYSIOTHERAPIST, PIZZA_HUT, PLAY_SCHOOL, PLAZA,
			PLUMBER, POLICE_STATION, POND, POST_OFFICE, POWER_PLANT, PRESS, PRINTING_STORE, PUB, RADIO_STATION,
			RAILWAY_STATION, RECEPTION, RECHARGE_SHOP, RECHARGE_STORE, REPAIR_SERVICE, RESORT, REST_ROOM, RESTAURANT,
			RICE_EXPORTER, ROAD, ROCK_SHOP, ROOM, RUBBER_STAMP, SALON, SCHOOL, SEPTIC_SYSTEM_SEVICE, SHOE_STORE,
			SHOPPING_MALL,SHOP, SHUTTLE, SKATING, SPORTS, SPORTS_CLUB, STALLS, STAMP_STORE, STATIONERY, STORE, SUBSTATION,
			SUPER_MARKET, SWEET_HOUSE, SWIMMING_POOL, TAILOR, TANK, TATTOO_SHOP, TAXI_SEVICE, TAXI_STAND, TEA_STALL,
			TELEPHONE_EXCHANGE, TEMPLE, TENNIS_COURT, TEXTILE_STORE, THEATRE, TICKET_COUNTER, TILE_STORE, TIMBER_STORE,
			TOOL_STORE, TOURS_AND_TRAVELS, TRAIN_STATION,TOWER, TUTION_CENTER, TV_REPAIR_SERVICE, TV_STATION, TV_TOWER,
			UNIVERSITY, VACATION_HOME_RENTAL_AGENCY, VALET_PARKING_SERVICE, VEGETABLE_MARKET, VEGETABLE_STORE,
			VEGETARIAN_RESTAURANT, VETERINARY_CARE, VIDEO_GAME_STORE, VISA_AGENT, VOLLEY_BALL_COURT,
			VOLUNTEER_ORGANISATION, WALLET_SHOP, WASH_ROOM, WATCH_REPAIR_SERVICE, WATCH_STORE, WATER_PLANT,
			WEB_SITE_DESIGNER, WEDDING_SERVICE, WELDER, WINE_STORE, X_RAY_EQUIPMENT_STORE, X_RAY_EQUIPMENT_SUPPLIER,
			X_RAY_LAB, XEROX_CENTRE, YOGA_CENTER, YOUTH_CLUB, ZOO
};
	
		public static final List<String> locationTypeList = Arrays.asList(LOCATION_TYPES);
		
		public static String getDefaultTypeForNonSpecifiedType(String locationtype) {
			// Given type doesn't exist, take as an other type
			if (!MarkConstants.locationTypeList.contains(locationtype)) {
				locationtype = TypeConstants.OTHER;
			}
			return locationtype;
		}
}

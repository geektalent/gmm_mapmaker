package com.geeklabs.gmm.markability.activity;

import static com.geeklabs.gmm.markability.util.TypeConstants.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.geeklabs.gmm.R;
import com.geeklabs.gmm.markability.gps.GPSTracker;
import com.geeklabs.gmm.markability.sqllite.MMSQLiteManager;
import com.geeklabs.gmm.markability.util.AppState;
import com.geeklabs.gmm.markability.util.MarkingInfo;

public class MarkFormSavingActivity extends Activity implements TextWatcher {

	private Button backbutton, savebutton;
	private EditText editText1, editText3;

	private AutoCompleteTextView autotext;
	String locationname, locationtype, description;
	
	// TODO - move them to constants class
	String type[] = { APARTMENT, AUDITORIUM, ATM, BAKERY, BAR, BANK, BUS_STOP, CAFE, COACHING_INSTITUTE, COLLEGE,
			CONSULTANCY, CHURCH, DHABA, ELECTRONICS, FUNCTION_HALL, GARDEN, HOTEL, HOSPITAL, INSTITUTE, INTERNETCAFE,
			LIBRARY, MARKET, MASJID, MEDICAL, OTHER, PARK, PETROL_BUNK, POLICE_STATION, PUB, PHARMACY, RAILWAY_STATION,
			RESTAURANT, RESORT, SALON, SCHOOL, SHOPPING_MALL, SPORTS, SUPER_MARKET, SWEET_HOUSE, TEMPLE, THEATRE,
			TOURS_AND_TRAVELS, XEROX_CENTRE, GYM, CANTEEN, PIZZA_HUT, STALLS, DINING_HALL, TEA_STALL, PARKING, ROOM,
			OFFICE, BLOCK, HOSTEL, TUTION_CENTER, HOME, POND, HOUSE, DRIVING_SCHOOL, FURNITURE, JEWELRY_STORE,
			RECEPTION, REST_ROOM, WASH_ROOM, TICKET_COUNTER, NOTICE_BOARD, ENQUIRY, UNIVERSITY, X_RAY_LAB, YOGA_CENTER,
			YOUTH_CLUB, COUNTRY_CLUB, ZOO, PLAZA, DANCE_SCHOOL, AEROBICS_SCHOOL, FAST_FOOD_CENTER,
			AIRLINE_TICKET_AGENCY, AIR_PORT, FUEL_STATION, WELDER, COMPLEX, BANQUET_HALL, BARBER_SHOP, BATTERY_STORE,
			BEACH, POST_OFFICE, CALL_CENTER, ADVERTISING_AGENCY, CAMERA_STORE, CHARITY_TRUST, CARPENTER,
			DAY_CARE_CENTER, DENTAL_CLINIC, DENTAL_COLLEGE, DHARMASHALA, LAMINATION_STORE, GAS_STATION, WATER_PLANT,
			POWER_PLANT, SUBSTATION, E_SEVA, GOVERNMENT_OFFICE, FIVE_STAR_HOTEL, GENERAL_STORE, COURT, CATERING,
			GIFT_SHOP, HAIR_SALON, HARDWARE_STORE, HEALTH_CLUB, ICE_CREAM_PARLOUR, JUICE_SHOP, SPORTS_CLUB,
			KIDS_STORE, LABORATORY, LAPTOP_STORE, RADIO_STATION, TV_STATION, TV_TOWER, MEDICAL_CLINIC, MEDICAL_CENTER,
			NEWS_SERVICE, NOVELTIES_STORE, PALACE, PAINTING, PAPER_DISTRIBUTOR, PHOTO_SHOP, PHOTO_STUDIO, TANK, TAILOR,
			TATTOO_SHOP, CLOTH_STORE, TAXI_SEVICE, TEXTILE_STORE, TELEPHONE_EXCHANGE, SWIMMING_POOL, ADDRESS,
			BABY_STORE, LADIES_EMPORIUM, BELT_SHOP, BLOOD_BANK, CAMP, CAR_RENTAL, CELL_PHONE_STORE, RECHARGE_STORE,
			CHEMICAL_PLANT, INDUSTRY, DAM, DJ, DOLL_STORE, EDUCATIONAL_CONSULTANT, LOAN_AGENCY, EGG_SUPPLIER,
			ELECTRICITY_BOARD, EMPLOYMENT_AGENCY, ENGINEERING_COLLEGE, DEGREE_COLLEGE, JUNIOR_COLLEGE, DENTAL_COLLEGE,
			MEDICAL_COLLEGE, LAW_SCHOOL, LAW_LIBRARY, LAW_BOOK_STORE, FASHION_DESIGNER, ANIMATION_SCHOOL, FIRE_STATION,
			FIRST_AID_SERVICE, FLORIST, FOOD_COURT, GAME_STORE, GENERAL_BOOK_STORE, ORPHANAGE, GIRLS_HOSTEL,
			OPTICAL_STORE, GLASS_REPAIR_STORE, HIGH_SCHOOL, ICSE_SCHOOL, CBSE_SCHOOL, BEAUTY_PARLOUR, PLAY_SCHOOL,
			INCOME_TAX_OFFICE, INSURANCE_AGENCY, INTELLIGENCE_AGENCY, IMAX_THEATRE, INTERIOR_DESIGNER, CURRY_POINT,
			JUICE_SHOP, KITE_SHOP, LABOUR_UNION, LADIES_TAILOR, MENS_TAILOR, MENS_HOSTEL, MENS_CLOTHING_STORE,
			MARBLE_STORE, MAGICIAN, NON_VEG_RESTAURANT, SEPTIC_SYSTEM_SEVICE, INVITATION_SERVICE, WINE_STORE,
			X_RAY_EQUIPMENT_STORE, WEDDING_SERVICE, WEB_SITE_DESIGNER, WATCH_REPAIR_SERVICE, WATCH_STORE, STORE,
			VEGETABLE_STORE, VEGETABLE_MARKET, VOLUNTEER_ORGANISATION, ELECTRICAL_STORE, TOOL_STORE, TIMBER_STORE,
			TILE_STORE, SHOE_STORE, FOOT_WEAR, CLUB, STAMP_STORE, RUBBER_STAMP, NOODLE_SHOP, OPEN_UNIVERSITY,
			OXYGEN_EQUIPMENT_SUPPLIER, ORCHARD, PASSPORT_OFFICE, PET_CARE, RICE_EXPORTER, ROCK_SHOP, GROUND,
			AGRICULTURE_SERVICE, REPAIR_SERVICE, AC_REPAIR_SERVICE, CAR_REPAIR_SERVICE, TV_REPAIR_SERVICE,
			FRIDGE_REPAIR_SERVICE, BAG_REPAIR_SERVICE, ELECTRONIC_GADGET_SERVICE, CRICKET_GROUND, TENNIS_COURT,
			BADMINTON_COURT, BASKET_BALL_COURT, BASE_BALL_COURT, GOLF, VOLLEY_BALL_COURT, HOCKEY_GROUND, SHUTTLE,
			SKATING, KINDERGARTEN, COURIER_OFFICE, PRINTING_STORE,LAUNDRY,GOLD_SHOP };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mark_form);

		addListenerOnClickButton();
		addListenerOnTextChange();

		savebutton.setEnabled(false);

		autotext = (AutoCompleteTextView) findViewById(R.id.locationtype);

		autotext.addTextChangedListener(this);
		autotext.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, type));
		
		MarkingInfo markingInfo = (MarkingInfo) getIntent().getSerializableExtra("markingInfo");
		if(markingInfo != null){
			editText1 = (EditText) findViewById(R.id.locationname);
			editText1.setText(markingInfo.getName());		
			autotext = (AutoCompleteTextView) findViewById(R.id.locationtype);
			autotext.setText(markingInfo.getType());
			
			editText3 = (EditText) findViewById(R.id.description);
			editText3.setText(markingInfo.getDescription());						
		}
		
	}

	private void addListenerOnTextChange() {
		editText1 = (EditText) findViewById(R.id.locationname);

		editText1.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (editText1.getText().length() > 0 && isValid(editText1.getText().toString().trim())) {
					savebutton.setEnabled(true);
				} else {
					savebutton.setEnabled(false);
					Toast.makeText(getApplicationContext(),
							"Oops!! Invalid Characters " + "Only allowed 0-9,_,a-z,A_Z.", Toast.LENGTH_SHORT).show();
					return;
				}
			}

			public boolean isValid(String str) {
				boolean isValid = false;
				String expression = "^[a-z_A-Z 0-9]*$";
				CharSequence inputStr = str;
				Pattern pattern = Pattern.compile(expression);
				Matcher matcher = pattern.matcher(inputStr);
				if (matcher.matches()) {
					isValid = true;
				}
				return isValid;
			}

		});

	}

	public void addListenerOnClickButton() {

		backbutton = (Button) findViewById(R.id.button1);
		savebutton = (Button) findViewById(R.id.button2);

		backbutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(MarkFormSavingActivity.this, MainMapActivity.class));
			}
		});

		savebutton.setOnClickListener(new OnClickListener() {

			

			@Override
			public void onClick(View v) {

				// DataBase storage

				editText3 = (EditText) findViewById(R.id.description);
				locationname = editText1.getText().toString();
				locationtype = autotext.getText().toString();
				description = editText3.getText().toString();
				
				GPSTracker gpsTracker = new GPSTracker(getApplicationContext());

				Double tappedLatitude = getIntent().getDoubleExtra("latitude", 0.0);
				Double tappedLongitude = getIntent().getDoubleExtra("longitude", 0.0);
				int zoomLevel = getIntent().getIntExtra("zoomLevel", 12);
				
				double actualLatitude = gpsTracker.getLatitude();
				double actualLongitude = gpsTracker.getLongitude();

				if (tappedLatitude != 0.0 && tappedLongitude != 0.0) {
					actualLatitude = tappedLatitude;
					actualLongitude = tappedLongitude;
				}

				MarkingInfo markingInfo = (MarkingInfo) getIntent().getSerializableExtra("markingInfo");
				if(markingInfo != null){
					MMSQLiteManager.updateMarkingInfo(openOrCreateDatabase("GMM", MODE_PRIVATE, null), locationname, locationtype, description, zoomLevel, markingInfo.getId());
				} else {					
					MMSQLiteManager.saveMarkingInfo(openOrCreateDatabase("GMM", MODE_PRIVATE, null), actualLatitude,
							actualLongitude, locationname, locationtype, description, zoomLevel);
				}
				
				if (!AppState.INSTANCE.appgenFQN.isEmpty()) {
					Intent i = null;
					try {
						i = new Intent(getApplicationContext(), Class.forName(AppState.INSTANCE.appgenFQN));
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
						
					}
					startActivity(i);
					
				} else {
					Intent intent = new Intent(getApplicationContext(), MainMapActivity.class);
					intent.putExtra("zoomLevel", zoomLevel);
					startActivity(intent);
							}
			}
		});
	}

	@Override
	public void afterTextChanged(Editable s) {

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

}
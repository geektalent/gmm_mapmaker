package com.geeklabs.mapmaker.activity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.geeklabs.mapmaker.R;
import com.geeklabs.mapmaker.sqllite.SQLLiteManager;
import com.geeklabs.mapmaker.util.AppState;
import com.geeklabs.mapmaker.util.MarkConstants;
import com.geeklabs.mapmaker.util.MarkingInfo;
import com.geeklabs.mapmaker.util.StringUtils;

public class MarkerSaveFormActivity extends Activity implements TextWatcher {

	private Button saveButton;
	private EditText locationNameEditText, descriptionEditText;

	private AutoCompleteTextView autotext;
	String locationname, locationtype, description;

	@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
	} 
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mark_form);

		// To get auto key soft keyboard on
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		// To save marking info in Database
		addListenerOnClickButton();
		// To Edit Location name in marking info
		addListenerOnTextChangeForLocationName();
		// To edit description in marking info
		addListenerOnTextChangeForDescription();

		saveButton.setEnabled(false);

		autotext = (AutoCompleteTextView) findViewById(R.id.locationtype);

		autotext.addTextChangedListener(this);
		autotext.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,
				MarkConstants.LOCATION_TYPES));
		MarkingInfo markingInfo = (MarkingInfo) getIntent().getSerializableExtra("markingInfo");
		if (markingInfo != null) {
			locationNameEditText = (EditText) findViewById(R.id.locationname);
			locationNameEditText.setText(markingInfo.getName());
			locationNameEditText.setSelection(locationNameEditText.getText().length());
			autotext = (AutoCompleteTextView) findViewById(R.id.locationtype);
			autotext.setText(markingInfo.getType());

			descriptionEditText = (EditText) findViewById(R.id.description);
			descriptionEditText.setText(markingInfo.getDescription());
		}

		autotext.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				autotext.showDropDown();
				return false;
			}
		});

		if (locationNameEditText.getText().toString().equals("")) {
			locationNameEditText.setError("Required field");
		}
	}

	// To Edit Location name in marking info
	private void addListenerOnTextChangeForLocationName() {
		locationNameEditText = (EditText) findViewById(R.id.locationname);

		locationNameEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (locationNameEditText.getText().length() > 0 && isValid(locationNameEditText.getText().toString().trim())) {
					saveButton.setEnabled(true);
					locationNameEditText.setError(null);
					Log.i("Location name changed", "Successfully location name is changed");
				} else {
					saveButton.setEnabled(false);
					locationNameEditText.setError("Required field");
					locationNameEditText.setError("Oops!! Invalid Characters " + "Only allowed 0-9,_,a-z,A_Z.");
					return;
				}
			}

			public boolean isValid(String str) {
				boolean isValid = false;
				String expression = "^[a-z_A-Z 0-9]*$";
				CharSequence inputStr = str;
				Pattern pattern = Pattern.compile(expression);
				Matcher matcher = pattern.matcher(inputStr);
				if (matcher.matches()) {
					isValid = true;
				}
				return isValid;
			}

		});

	}

	// To edit description in marking info
	private void addListenerOnTextChangeForDescription() {
		descriptionEditText = (EditText) findViewById(R.id.description);
		descriptionEditText.addTextChangedListener(new TextWatcher() {
			EditText des = descriptionEditText;
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (des.getText().length() > 50) {
					des.setError("Only 50 Characters allowed");
				}
				// des.setError(null);
			}
		});
	}

	// To save marking info in Database
	public void addListenerOnClickButton() {
		saveButton = (Button) findViewById(R.id.button2);
		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				// DataBase storage
				saveButton.setBackgroundColor(Color.rgb(49, 140, 231));
				descriptionEditText = (EditText) findViewById(R.id.description);

				// Don't allow space in front and end of the word
				locationname = StringUtils.trim(locationNameEditText.getText().toString());
				locationtype = StringUtils.trim(autotext.getText().toString());
				description = StringUtils.trim(descriptionEditText.getText().toString());

				Double tappedLatitude = getIntent().getDoubleExtra("latitude", 0.0);
				Double tappedLongitude = getIntent().getDoubleExtra("longitude", 0.0);
				String markedBy = getIntent().getStringExtra("MarkedBy");
				float zoomLevel = getIntent().getFloatExtra("zoomLevel", 12);

				Double myLocLatitude = getIntent().getDoubleExtra("myLocationLatitude", 0.0);
				Double myLocLongitude = getIntent().getDoubleExtra("myLocationLongitude", 0.0);

				if (tappedLatitude != 0.0 && tappedLongitude != 0.0) {
					myLocLatitude = tappedLatitude;
					myLocLongitude = tappedLongitude;
				}

				MarkingInfo markingInfo = (MarkingInfo) getIntent().getSerializableExtra("markingInfo");
				if (markingInfo != null) {
					SQLLiteManager.getInstance(getApplicationContext()).updateMarkingInfo(locationname, locationtype,
							description, zoomLevel, markingInfo.getId());
					myLocLatitude = markingInfo.getLattitude();
					myLocLongitude = markingInfo.getLongtitutde();
					Log.i("Update marking info", "Marking info successfully updated");
				} else {
					SQLLiteManager.getInstance(getApplicationContext()).saveMarkingInfo(myLocLatitude, myLocLongitude,
							locationname, locationtype, description, markedBy, zoomLevel);
					Log.i("Save marking info", "Marking info successfully saved");
				}

				if (!AppState.INSTANCE.appgenFQN.isEmpty()) {
					setResult(RESULT_OK);
					finish();
				} else {
					setResult(RESULT_OK);
					finish();
				}
			}
		});
	}

	@Override
	public void afterTextChanged(Editable s) {

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

}
